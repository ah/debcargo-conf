diff --git a/Cargo.toml b/Cargo.toml
index fcef654..eb96fc8 100644
--- a/Cargo.toml
+++ b/Cargo.toml
@@ -43,7 +43,7 @@ proc-macro = true
 version = "1.0.11"
 
 [dependencies.quick-xml]
-version = "0.23"
+version = "0.27"
 
 [dependencies.quote]
 version = "1.0"
diff --git a/src/parse.rs b/src/parse.rs
index 825ec5e823..b07c6531cc 100644
--- a/src/parse.rs
+++ b/src/parse.rs
@@ -11,7 +11,7 @@ use quick_xml::{
 
 macro_rules! extract_from(
     ($it: expr => $pattern: pat => $result: tt) => (
-        match $it.read_event(&mut Vec::new()) {
+        match $it.read_event_into(&mut Vec::new()) {
             Ok($pattern) => { $result },
             e => panic!("Ill-formed protocol file: {:?}", e)
         }
@@ -21,7 +21,7 @@ macro_rules! extract_from(
 macro_rules! extract_end_tag(
     ($it: expr => $tag: expr) => (
         extract_from!($it => Event::End(bytes) => {
-            assert!(bytes.name() == $tag.as_bytes(), "Ill-formed protocol file");
+            assert!(bytes.name().into_inner() == $tag.as_bytes(), "Ill-formed protocol file");
         });
     )
 );
@@ -30,7 +30,7 @@ pub fn parse<S: Read>(stream: S) -> Protocol {
     let mut reader = Reader::from_reader(BufReader::new(stream));
     reader.trim_text(true).expand_empty_elements(true);
     // Skip first <?xml ... ?> event
-    let _ = reader.read_event(&mut Vec::new());
+    let _ = reader.read_event_into(&mut Vec::new());
     parse_protocol(reader)
 }
 
@@ -55,8 +55,8 @@ fn parse_or_panic<T: FromStr>(txt: &[u8]) -> T {
 fn parse_protocol<R: BufRead>(mut reader: Reader<R>) -> Protocol {
     let mut protocol = extract_from!(
         reader => Event::Start(bytes) => {
-            assert!(bytes.name() == b"protocol", "Missing protocol toplevel tag");
-            if let Some(attr) = bytes.attributes().filter_map(|res| res.ok()).find(|attr| attr.key == b"name") {
+            assert!(bytes.name().into_inner() == b"protocol", "Missing protocol toplevel tag");
+            if let Some(attr) = bytes.attributes().filter_map(|res| res.ok()).find(|attr| attr.key.into_inner() == b"name") {
                 Protocol::new(decode_utf8_or_panic(attr.value.into_owned()))
             } else {
                 panic!("Protocol must have a name");
@@ -65,14 +65,14 @@ fn parse_protocol<R: BufRead>(mut reader: Reader<R>) -> Protocol {
     );
 
     loop {
-        match reader.read_event(&mut Vec::new()) {
+        match reader.read_event_into(&mut Vec::new()) {
             Ok(Event::Start(bytes)) => {
-                match bytes.name() {
+                match bytes.name().into_inner() {
                     b"copyright" => {
                         // parse the copyright
-                        let copyright = match reader.read_event(&mut Vec::new()) {
+                        let copyright = match reader.read_event_into(&mut Vec::new()) {
                             Ok(Event::Text(copyright)) => {
-                                copyright.unescape_and_decode(&reader).ok()
+                                copyright.unescape().ok().map(|x| x.to_string())
                             }
                             Ok(Event::CData(copyright)) => {
                                 String::from_utf8(copyright.into_inner().into()).ok()
@@ -90,18 +90,19 @@ fn parse_protocol<R: BufRead>(mut reader: Reader<R>) -> Protocol {
                         protocol.description =
                             Some(parse_description(&mut reader, bytes.attributes()));
                     }
-                    _ => panic!(
+                    name => panic!(
                         "Ill-formed protocol file: unexpected token `{}` in protocol {}",
-                        String::from_utf8_lossy(bytes.name()),
+                        String::from_utf8_lossy(name),
                         protocol.name
                     ),
                 }
             }
             Ok(Event::End(bytes)) => {
+                let name = bytes.name().into_inner();
                 assert!(
-                    bytes.name() == b"protocol",
+                    name == b"protocol",
                     "Unexpected closing token `{}`",
-                    String::from_utf8_lossy(bytes.name())
+                    String::from_utf8_lossy(name)
                 );
                 break;
             }
@@ -117,7 +118,7 @@ fn parse_protocol<R: BufRead>(mut reader: Reader<R>) -> Protocol {
 fn parse_interface<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> Interface {
     let mut interface = Interface::new();
     for attr in attrs.filter_map(|res| res.ok()) {
-        match attr.key {
+        match attr.key.into_inner() {
             b"name" => interface.name = decode_utf8_or_panic(attr.value.into_owned()),
             b"version" => interface.version = parse_or_panic(&attr.value),
             _ => {}
@@ -125,17 +126,17 @@ fn parse_interface<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> Int
     }
 
     loop {
-        match reader.read_event(&mut Vec::new()) {
-            Ok(Event::Start(bytes)) => match bytes.name() {
+        match reader.read_event_into(&mut Vec::new()) {
+            Ok(Event::Start(bytes)) => match bytes.name().into_inner() {
                 b"description" => {
                     interface.description = Some(parse_description(reader, bytes.attributes()))
                 }
                 b"request" => interface.requests.push(parse_request(reader, bytes.attributes())),
                 b"event" => interface.events.push(parse_event(reader, bytes.attributes())),
                 b"enum" => interface.enums.push(parse_enum(reader, bytes.attributes())),
-                _ => panic!("Unexpected token: `{}`", String::from_utf8_lossy(bytes.name())),
+                name => panic!("Unexpected token: `{}`", String::from_utf8_lossy(name)),
             },
-            Ok(Event::End(bytes)) if bytes.name() == b"interface" => break,
+            Ok(Event::End(bytes)) if bytes.name().into_inner() == b"interface" => break,
             _ => {}
         }
     }
@@ -146,7 +147,7 @@ fn parse_interface<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> Int
 fn parse_description<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> (String, String) {
     let mut summary = String::new();
     for attr in attrs.filter_map(|res| res.ok()) {
-        if attr.key == b"summary" {
+        if attr.key.into_inner() == b"summary" {
             summary = String::from_utf8_lossy(&attr.value)
                 .split_whitespace()
                 .collect::<Vec<_>>()
@@ -158,14 +159,14 @@ fn parse_description<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> (
     // Some protocols have comments inside their descriptions, so we need to parse them in a loop and
     // concatenate the parts into a single block of text
     loop {
-        match reader.read_event(&mut Vec::new()) {
+        match reader.read_event_into(&mut Vec::new()) {
             Ok(Event::Text(bytes)) => {
                 if !description.is_empty() {
                     description.push_str("\n\n");
                 }
-                description.push_str(&bytes.unescape_and_decode(reader).unwrap_or_default())
+                description.push_str(&bytes.unescape().unwrap_or_default())
             }
-            Ok(Event::End(bytes)) if bytes.name() == b"description" => break,
+            Ok(Event::End(bytes)) if bytes.name().into_inner() == b"description" => break,
             Ok(Event::Comment(_)) => {}
             e => panic!("Ill-formed protocol file: {:?}", e),
         }
@@ -177,7 +178,7 @@ fn parse_description<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> (
 fn parse_request<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> Message {
     let mut request = Message::new();
     for attr in attrs.filter_map(|res| res.ok()) {
-        match attr.key {
+        match attr.key.into_inner() {
             b"name" => request.name = decode_utf8_or_panic(attr.value.into_owned()),
             b"type" => request.typ = Some(parse_type(&attr.value)),
             b"since" => request.since = parse_or_panic(&attr.value),
@@ -186,15 +187,15 @@ fn parse_request<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> Messa
     }
 
     loop {
-        match reader.read_event(&mut Vec::new()) {
-            Ok(Event::Start(bytes)) => match bytes.name() {
+        match reader.read_event_into(&mut Vec::new()) {
+            Ok(Event::Start(bytes)) => match bytes.name().into_inner() {
                 b"description" => {
                     request.description = Some(parse_description(reader, bytes.attributes()))
                 }
                 b"arg" => request.args.push(parse_arg(reader, bytes.attributes())),
-                _ => panic!("Unexpected token: `{}`", String::from_utf8_lossy(bytes.name())),
+                name => panic!("Unexpected token: `{}`", String::from_utf8_lossy(name)),
             },
-            Ok(Event::End(bytes)) if bytes.name() == b"request" => break,
+            Ok(Event::End(bytes)) if bytes.name().into_inner() == b"request" => break,
             _ => {}
         }
     }
@@ -205,7 +206,7 @@ fn parse_request<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> Messa
 fn parse_enum<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> Enum {
     let mut enu = Enum::new();
     for attr in attrs.filter_map(|res| res.ok()) {
-        match attr.key {
+        match attr.key.into_inner() {
             b"name" => enu.name = decode_utf8_or_panic(attr.value.into_owned()),
             b"since" => enu.since = parse_or_panic(&attr.value),
             b"bitfield" => {
@@ -218,15 +219,15 @@ fn parse_enum<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> Enum {
     }
 
     loop {
-        match reader.read_event(&mut Vec::new()) {
-            Ok(Event::Start(bytes)) => match bytes.name() {
+        match reader.read_event_into(&mut Vec::new()) {
+            Ok(Event::Start(bytes)) => match bytes.name().into_inner() {
                 b"description" => {
                     enu.description = Some(parse_description(reader, bytes.attributes()))
                 }
                 b"entry" => enu.entries.push(parse_entry(reader, bytes.attributes())),
-                _ => panic!("Unexpected token: `{}`", String::from_utf8_lossy(bytes.name())),
+                name => panic!("Unexpected token: `{}`", String::from_utf8_lossy(name)),
             },
-            Ok(Event::End(bytes)) if bytes.name() == b"enum" => break,
+            Ok(Event::End(bytes)) if bytes.name().into_inner() == b"enum" => break,
             _ => {}
         }
     }
@@ -237,7 +238,7 @@ fn parse_enum<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> Enum {
 fn parse_event<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> Message {
     let mut event = Message::new();
     for attr in attrs.filter_map(|res| res.ok()) {
-        match attr.key {
+        match attr.key.into_inner() {
             b"name" => event.name = decode_utf8_or_panic(attr.value.into_owned()),
             b"type" => event.typ = Some(parse_type(&attr.value)),
             b"since" => event.since = parse_or_panic(&attr.value),
@@ -246,15 +247,15 @@ fn parse_event<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> Message
     }
 
     loop {
-        match reader.read_event(&mut Vec::new()) {
-            Ok(Event::Start(bytes)) => match bytes.name() {
+        match reader.read_event_into(&mut Vec::new()) {
+            Ok(Event::Start(bytes)) => match bytes.name().into_inner() {
                 b"description" => {
                     event.description = Some(parse_description(reader, bytes.attributes()))
                 }
                 b"arg" => event.args.push(parse_arg(reader, bytes.attributes())),
-                _ => panic!("Unexpected token: `{}`", String::from_utf8_lossy(bytes.name())),
+                name => panic!("Unexpected token: `{}`", String::from_utf8_lossy(name)),
             },
-            Ok(Event::End(bytes)) if bytes.name() == b"event" => break,
+            Ok(Event::End(bytes)) if bytes.name().into_inner() == b"event" => break,
             _ => {}
         }
     }
@@ -265,7 +266,7 @@ fn parse_event<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> Message
 fn parse_arg<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> Arg {
     let mut arg = Arg::new();
     for attr in attrs.filter_map(|res| res.ok()) {
-        match attr.key {
+        match attr.key.into_inner() {
             b"name" => arg.name = decode_utf8_or_panic(attr.value.into_owned()),
             b"type" => arg.typ = parse_type(&attr.value),
             b"summary" => {
@@ -288,14 +289,14 @@ fn parse_arg<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> Arg {
     }
 
     loop {
-        match reader.read_event(&mut Vec::new()) {
-            Ok(Event::Start(bytes)) => match bytes.name() {
+        match reader.read_event_into(&mut Vec::new()) {
+            Ok(Event::Start(bytes)) => match bytes.name().into_inner() {
                 b"description" => {
                     arg.description = Some(parse_description(reader, bytes.attributes()))
                 }
-                _ => panic!("Unexpected token: `{}`", String::from_utf8_lossy(bytes.name())),
+                name => panic!("Unexpected token: `{}`", String::from_utf8_lossy(name)),
             },
-            Ok(Event::End(bytes)) if bytes.name() == b"arg" => break,
+            Ok(Event::End(bytes)) if bytes.name().into_inner() == b"arg" => break,
             _ => {}
         }
     }
@@ -321,7 +322,7 @@ fn parse_type(txt: &[u8]) -> Type {
 fn parse_entry<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> Entry {
     let mut entry = Entry::new();
     for attr in attrs.filter_map(|res| res.ok()) {
-        match attr.key {
+        match attr.key.into_inner() {
             b"name" => entry.name = decode_utf8_or_panic(attr.value.into_owned()),
             b"value" => {
                 entry.value = if attr.value.starts_with(b"0x") {
@@ -351,14 +352,14 @@ fn parse_entry<R: BufRead>(reader: &mut Reader<R>, attrs: Attributes) -> Entry {
     }
 
     loop {
-        match reader.read_event(&mut Vec::new()) {
-            Ok(Event::Start(bytes)) => match bytes.name() {
+        match reader.read_event_into(&mut Vec::new()) {
+            Ok(Event::Start(bytes)) => match bytes.name().into_inner() {
                 b"description" => {
                     entry.description = Some(parse_description(reader, bytes.attributes()))
                 }
-                _ => panic!("Unexpected token: `{}`", String::from_utf8_lossy(bytes.name())),
+                name => panic!("Unexpected token: `{}`", String::from_utf8_lossy(name)),
             },
-            Ok(Event::End(bytes)) if bytes.name() == b"entry" => break,
+            Ok(Event::End(bytes)) if bytes.name().into_inner() == b"entry" => break,
             _ => {}
         }
     }
