rust-syntect (5.1.0-2) unstable; urgency=medium

  * Team upload.
  * Package syntect 5.1.0 from crates.io using debcargo 2.6.0
  * Bump regex dependency to 0.8 and regex-syntax dependency to 0.10

 -- Peter Michael Green <plugwash@debian.org>  Tue, 24 Oct 2023 23:17:13 +0000

rust-syntect (5.1.0-1) unstable; urgency=medium

  * Team upload.
  * Package syntect 5.1.0 from crates.io using debcargo 2.6.0
  * Update patches for new upstream.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 05 Oct 2023 03:27:36 +0000

rust-syntect (5.0.0-2) unstable; urgency=medium

  * Team upload.
  * Package syntect 5.0.0 from crates.io using debcargo 2.6.0
  * Bump fancy-regex dependency to 0.11
  * Bump pretty-assertions dev-depencency to 1.0
  * Update disable-tests-missing-data.patch for version 5.0.0
    (the tests were skipped in the previous upload due to
    unsatisfiable pretty-assertions dev-dependency)
  * Drop drop-criterion.patch, criterion is now in Debian.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 04 Jul 2023 02:16:21 +0000

rust-syntect (5.0.0-1) unstable; urgency=medium

  * Team upload.
  * Package syntect 5.0.0 from crates.io using debcargo 2.5.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 12 Jun 2022 09:42:51 +0200

rust-syntect (4.6.0-1) unstable; urgency=medium

  * Package syntect 4.6.0 from crates.io using debcargo 2.5.0
  * Use collapse_features = true
  * Fix autopkgtest
    + Disable benches that depend on criterion so the rest of the testsuite
      can run.
    + Disable tests that require test data which is not included in the
      crates.io release.
    + Establish baseline for tests.

  [ Sylvestre Ledru ]
  * Team upload.
  * Package syntect 4.6.0 from crates.io using debcargo 2.4.4-alpha.0

 -- Peter Michael Green <plugwash@debian.org>  Sat, 15 Jan 2022 18:38:09 +0000

rust-syntect (3.3.0-4) unstable; urgency=medium

  * try to unbreak the build (Closes: #975175)

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 19 Nov 2020 21:30:20 +0100

rust-syntect (3.3.0-3) unstable; urgency=medium

  * Team upload.
  * Package syntect 3.3.0 from crates.io using debcargo 2.4.3
  * Build the plist dep to 1 (Closes: #971146)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 04 Oct 2020 17:16:42 +0200

rust-syntect (3.3.0-2) unstable; urgency=medium

  * Team upload.
  * Package syntect 3.3.0 from crates.io using debcargo 2.4.2

 -- Ximin Luo <infinity0@debian.org>  Wed, 08 Jan 2020 23:22:53 +0000

rust-syntect (3.3.0-1) unstable; urgency=medium

  * Package syntect 3.3.0 from crates.io using debcargo 2.4.0

 -- Paride Legovini <pl@ninthfloor.org>  Wed, 04 Dec 2019 00:17:23 +0000

rust-syntect (3.2.0-1) unstable; urgency=medium

  * Team upload.
  * Package syntect 3.2.0 from crates.io using debcargo 2.2.10

  [ Paride Legovini ]
  * Package syntect 2.1.0 from crates.io using debcargo 2.2.3

 -- Helen Koike <helen@koikeco.de>  Sat, 20 Jul 2019 16:30:06 -0300
