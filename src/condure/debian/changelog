rust-condure (1.10.0-3) unstable; urgency=medium

  * Team upload.
  * Package condure 1.10.0 from crates.io using debcargo 2.6.0
  * Bump dependency on miniz-oxide.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 28 Aug 2023 13:52:46 +0000

rust-condure (1.10.0-2) unstable; urgency=medium

  * Team upload.
  * Package condure 1.10.0 from crates.io using debcargo 2.6.0
  * Fix build on systems with unsigned char (Closes: #1040888)

 -- Peter Michael Green <plugwash@debian.org>  Wed, 12 Jul 2023 15:14:13 +0000

rust-condure (1.10.0-1) unstable; urgency=medium

  * Team upload.
  * Package condure 1.10.0 from crates.io using debcargo 2.6.0
  * Drop disable-lib.diff, it doesn't apply to the new upstream version
    and it seems ineffective anway.
  * Update base64-0.21.patch for new upstream.
  * Update overridden control file for new upstream.
  * Relax dependency on clap.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 11 Jul 2023 21:48:57 +0000

rust-condure (1.9.2-1) unstable; urgency=medium

  * Team upload.
  * Package condure 1.9.2 from crates.io using debcargo 2.6.0
  * Add patch for base64 0.21.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 13 Jun 2023 12:06:18 +0000

rust-condure (1.9.1-1) unstable; urgency=medium

  * Package condure 1.9.1 from crates.io using debcargo 2.6.0

 -- Jan Niehusmann <jan@debian.org>  Sun, 22 Jan 2023 21:18:12 +0000

rust-condure (1.6.0-3) unstable; urgency=medium

  * Team upload.
  * Package condure 1.6.0 from crates.io using debcargo 2.5.0
  * Set test_is_broken, the autopkgtest doesn't work because the lib package
    is stripped out from debian/control.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 05 Jul 2022 13:12:37 +0000

rust-condure (1.6.0-2) unstable; urgency=medium

  * Team upload.
  * Package condure 1.6.0 from crates.io using debcargo 2.5.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 04 Jun 2022 15:57:18 -0400

rust-condure (1.6.0-1) experimental; urgency=medium

  * Team upload.
  * Package condure 1.6.0 from crates.io using debcargo 2.5.0
  * Drop arrayvec-0.7 patch, applied upstream

 -- James McCoy <jamessan@debian.org>  Wed, 01 Jun 2022 10:08:59 -0400

rust-condure (1.3.1-1) unstable; urgency=medium

  * Team upload.
  * Package condure 1.3.1 from crates.io using debcargo 2.5.0
  * Update patches for new upstream
    + drop rustc-1.43-compat.diff no longer needed
    + update disable-lib.diff for changes in upstream Cargo.toml
    + rewrite arrayvec-0.7.patch for changes in upstream code

 -- Peter Michael Green <plugwash@debian.org>  Thu, 20 Jan 2022 22:48:23 +0000

rust-condure (1.1.0-4) unstable; urgency=medium

  * Team upload.
  * Package condure 1.1.0 from crates.io using debcargo 2.5.0
  * Add patch for arrayvec 0.7

 -- Peter Michael Green <plugwash@debian.org>  Tue, 21 Dec 2021 20:52:31 +0000

rust-condure (1.1.0-3) unstable; urgency=medium

  * Team upload.
  * Package condure 1.1.0 from crates.io using debcargo 2.4.4

 -- Peter Michael Green <plugwash@debian.org>  Wed, 01 Sep 2021 00:57:08 +0000

rust-condure (1.1.0-2) unstable; urgency=medium

  * Team upload.
  * Package condure 1.1.0 from crates.io using debcargo 2.4.4
  * Relax dependency on base64.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 22 Aug 2021 11:36:22 +0000

rust-condure (1.1.0-1) unstable; urgency=medium

  * Package condure 1.1.0 from crates.io using debcargo 2.4.3
    (Closes: Bug#975630)

 -- Jan Niehusmann <jan@debian.org>  Wed, 25 Nov 2020 20:25:37 +0100
