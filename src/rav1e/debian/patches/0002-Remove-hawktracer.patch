From: Sebastian Ramacher <sramacher@debian.org>
Date: Thu, 29 Dec 2022 11:14:32 +0100
Subject: Remove hawktracer

---
 Cargo.toml             | 4 ----
 src/activity.rs        | 3 ---
 src/api/internal.rs    | 9 ---------
 src/api/lookahead.rs   | 5 -----
 src/bin/muxer/ivf.rs   | 2 --
 src/bin/stats.rs       | 3 ---
 src/cdef.rs            | 2 --
 src/deblock.rs         | 4 ----
 src/encoder.rs         | 3 ---
 src/lrf.rs             | 2 --
 src/me.rs              | 3 ---
 src/scenechange/mod.rs | 5 -----
 12 files changed, 45 deletions(-)

--- a/Cargo.toml
+++ b/Cargo.toml
@@ -186,9 +186,6 @@
 default-features = false
 package = "maybe-rayon"
 
-[dependencies.rust_hawktracer]
-version = "0.7.0"
-
 [dependencies.scan_fmt]
 version = "0.2.3"
 optional = true
@@ -327,10 +324,6 @@
 ]
 signal_support = ["signal-hook"]
 threading = ["rayon/threads"]
-tracing = [
-    "rust_hawktracer/profiling_enabled",
-    "rust_hawktracer/pkg_config",
-]
 unstable = []
 wasm = ["wasm-bindgen"]
 
--- a/src/activity.rs
+++ b/src/activity.rs
@@ -12,7 +12,6 @@
 use crate::tiling::*;
 use crate::util::*;
 use itertools::izip;
-use rust_hawktracer::*;
 
 #[derive(Debug, Default, Clone)]
 pub struct ActivityMask {
@@ -20,7 +19,6 @@
 }
 
 impl ActivityMask {
-  #[hawktracer(ActivityMask_from_plane)]
   pub fn from_plane<T: Pixel>(luma_plane: &Plane<T>) -> ActivityMask {
     let PlaneConfig { width, height, .. } = luma_plane.cfg;
 
@@ -55,7 +53,6 @@
     ActivityMask { variances: variances.into_boxed_slice() }
   }
 
-  #[hawktracer(ActivityMask_fill_scales)]
   pub fn fill_scales(
     &self, bit_depth: usize, activity_scales: &mut Box<[DistortionScale]>,
   ) {
--- a/src/api/internal.rs
+++ b/src/api/internal.rs
@@ -28,7 +28,6 @@
 use crate::tiling::Area;
 use crate::util::Pixel;
 use arrayvec::ArrayVec;
-use rust_hawktracer::*;
 use std::cmp;
 use std::collections::{BTreeMap, BTreeSet};
 use std::env;
@@ -316,7 +315,6 @@
     }
   }
 
-  #[hawktracer(send_frame)]
   pub fn send_frame(
     &mut self, mut frame: Option<Arc<Frame<T>>>,
     params: Option<FrameParameters>,
@@ -649,7 +647,6 @@
   /// `rec_buffer` and `lookahead_rec_buffer` on the `FrameInvariants`. This
   /// function must be called after every new `FrameInvariants` is initially
   /// computed.
-  #[hawktracer(compute_lookahead_motion_vectors)]
   fn compute_lookahead_motion_vectors(&mut self, output_frameno: u64) {
     let frame_data = self.frame_data.get(&output_frameno).unwrap();
 
@@ -862,7 +859,6 @@
       });
   }
 
-  #[hawktracer(compute_keyframe_placement)]
   pub fn compute_keyframe_placement(
     lookahead_frames: &[&Arc<Frame<T>>], keyframes_forced: &BTreeSet<u64>,
     keyframe_detector: &mut SceneChangeDetector<T>,
@@ -881,7 +877,6 @@
     *next_lookahead_frame += 1;
   }
 
-  #[hawktracer(compute_frame_invariants)]
   pub fn compute_frame_invariants(&mut self) {
     while self.set_frame_properties(self.next_lookahead_output_frameno).is_ok()
     {
@@ -894,7 +889,6 @@
     }
   }
 
-  #[hawktracer(update_block_importances)]
   fn update_block_importances(
     fi: &FrameInvariants<T>, me_stats: &crate::me::FrameMEStats,
     frame: &Frame<T>, reference_frame: &Frame<T>, bit_depth: usize,
@@ -1057,7 +1051,6 @@
   }
 
   /// Computes the block importances for the current output frame.
-  #[hawktracer(compute_block_importances)]
   fn compute_block_importances(&mut self) {
     // SEF don't need block importances.
     if self.frame_data[&self.output_frameno]
@@ -1281,7 +1274,6 @@
     }
   }
 
-  #[hawktracer(encode_show_existing_packet)]
   pub fn encode_show_existing_packet(
     &mut self, cur_output_frameno: u64,
   ) -> Result<Packet<T>, EncoderStatus> {
@@ -1317,7 +1309,6 @@
     self.finalize_packet(rec, source, input_frameno, frame_type, qp, enc_stats)
   }
 
-  #[hawktracer(encode_normal_packet)]
   pub fn encode_normal_packet(
     &mut self, cur_output_frameno: u64,
   ) -> Result<Packet<T>, EncoderStatus> {
@@ -1480,7 +1471,6 @@
     }
   }
 
-  #[hawktracer(receive_packet)]
   pub fn receive_packet(&mut self) -> Result<Packet<T>, EncoderStatus> {
     if self.done_processing() {
       return Err(EncoderStatus::LimitReached);
@@ -1546,7 +1536,6 @@
     })
   }
 
-  #[hawktracer(garbage_collect)]
   fn garbage_collect(&mut self, cur_input_frameno: u64) {
     if cur_input_frameno == 0 {
       return;
--- a/src/api/lookahead.rs
+++ b/src/api/lookahead.rs
@@ -14,7 +14,6 @@
 use crate::tiling::{Area, PlaneRegion, TileRect};
 use crate::transform::TxSize;
 use crate::Pixel;
-use rust_hawktracer::*;
 use std::sync::Arc;
 use v_frame::frame::Frame;
 use v_frame::pixel::CastFromPrimitive;
@@ -26,7 +25,6 @@
 pub(crate) const IMP_BLOCK_AREA_IN_MV_UNITS: i64 =
   IMP_BLOCK_SIZE_IN_MV_UNITS * IMP_BLOCK_SIZE_IN_MV_UNITS;
 
-#[hawktracer(estimate_intra_costs)]
 pub(crate) fn estimate_intra_costs<T: Pixel>(
   temp_plane: &mut Plane<T>, frame: &Frame<T>, bit_depth: usize,
   cpu_feature_level: CpuFeatureLevel,
@@ -120,7 +118,6 @@
   intra_costs.into_boxed_slice()
 }
 
-#[hawktracer(estimate_importance_block_difference)]
 pub(crate) fn estimate_importance_block_difference<T: Pixel>(
   frame: Arc<Frame<T>>, ref_frame: Arc<Frame<T>>,
 ) -> f64 {
@@ -176,7 +173,6 @@
   imp_block_costs as f64 / (w_in_imp_b * h_in_imp_b) as f64
 }
 
-#[hawktracer(estimate_inter_costs)]
 pub(crate) fn estimate_inter_costs<T: Pixel>(
   frame: Arc<Frame<T>>, ref_frame: Arc<Frame<T>>, bit_depth: usize,
   mut config: EncoderConfig, sequence: Arc<Sequence>, buffer: RefMEStats,
@@ -265,7 +261,6 @@
   inter_costs as f64 / (w_in_imp_b * h_in_imp_b) as f64
 }
 
-#[hawktracer(compute_motion_vectors)]
 pub(crate) fn compute_motion_vectors<T: Pixel>(
   fi: &mut FrameInvariants<T>, fs: &mut FrameState<T>, inter_cfg: &InterConfig,
 ) {
--- a/src/bin/muxer/ivf.rs
+++ b/src/bin/muxer/ivf.rs
@@ -12,7 +12,6 @@
 use crate::error::*;
 use ivf::*;
 use rav1e::prelude::*;
-use rust_hawktracer::*;
 use std::fs;
 use std::fs::File;
 use std::io;
@@ -37,7 +36,6 @@
     );
   }
 
-  #[hawktracer(write_frame)]
   fn write_frame(&mut self, pts: u64, data: &[u8], _frame_type: FrameType) {
     write_ivf_frame(&mut self.output, pts, data);
   }
--- a/src/bin/stats.rs
+++ b/src/bin/stats.rs
@@ -12,7 +12,6 @@
 use rav1e::prelude::Rational;
 use rav1e::prelude::*;
 use rav1e::{Packet, Pixel};
-use rust_hawktracer::*;
 use std::fmt;
 use std::time::Instant;
 
@@ -30,7 +29,6 @@
   pub enc_stats: EncoderStats,
 }
 
-#[hawktracer(build_frame_summary)]
 pub fn build_frame_summary<T: Pixel>(
   packets: Packet<T>, bit_depth: usize, chroma_sampling: ChromaSampling,
   metrics_cli: MetricsEnabled,
--- a/src/cdef.rs
+++ b/src/cdef.rs
@@ -13,7 +13,6 @@
 use crate::frame::*;
 use crate::tiling::*;
 use crate::util::{clamp, msb, CastFromPrimitive, Pixel};
-use rust_hawktracer::*;
 
 use crate::cpu_features::CpuFeatureLevel;
 use std::cmp;
@@ -322,7 +321,6 @@
   }
 }
 
-#[hawktracer(cdef_analyze_superblock_range)]
 pub fn cdef_analyze_superblock_range<T: Pixel>(
   fi: &FrameInvariants<T>, in_frame: &Frame<T>, blocks: &TileBlocks<'_>,
   sb_w: usize, sb_h: usize,
@@ -337,7 +335,6 @@
   ret
 }
 
-#[hawktracer(cdef_analyze_superblock)]
 pub fn cdef_analyze_superblock<T: Pixel>(
   fi: &FrameInvariants<T>, in_frame: &Frame<T>, blocks: &TileBlocks<'_>,
   sbo: TileSuperBlockOffset,
@@ -398,7 +395,6 @@
 /// # Panics
 ///
 /// - If called with invalid parameters
-#[hawktracer(cdef_filter_superblock)]
 pub fn cdef_filter_superblock<T: Pixel>(
   fi: &FrameInvariants<T>, input: &Frame<T>, output: &mut TileMut<'_, T>,
   blocks: &TileBlocks<'_>, tile_sbo: TileSuperBlockOffset, cdef_index: u8,
@@ -594,7 +590,6 @@
 //   tile boundary), the filtering process ignores input pixels that
 //   don't exist.
 
-#[hawktracer(cdef_filter_tile)]
 pub fn cdef_filter_tile<T: Pixel>(
   fi: &FrameInvariants<T>, input: &Frame<T>, tb: &TileBlocks,
   output: &mut TileMut<'_, T>,
--- a/src/deblock.rs
+++ b/src/deblock.rs
@@ -18,7 +18,6 @@
 use crate::tiling::*;
 use crate::util::{clamp, ILog, Pixel};
 use crate::DeblockState;
-use rust_hawktracer::*;
 use std::cmp;
 
 fn deblock_adjusted_level(
@@ -1291,7 +1290,6 @@
 }
 
 // Deblocks all edges, vertical and horizontal, in a single plane
-#[hawktracer(deblock_plane)]
 pub fn deblock_plane<T: Pixel>(
   deblock: &DeblockState, p: &mut PlaneRegionMut<T>, pli: usize,
   blocks: &TileBlocks, crop_w: usize, crop_h: usize, bd: usize,
@@ -1541,7 +1539,6 @@
 }
 
 // Deblocks all edges in all planes of a frame
-#[hawktracer(deblock_filter_frame)]
 pub fn deblock_filter_frame<T: Pixel>(
   deblock: &DeblockState, tile: &mut TileMut<T>, blocks: &TileBlocks,
   crop_w: usize, crop_h: usize, bd: usize, planes: usize,
@@ -1617,7 +1614,6 @@
   level
 }
 
-#[hawktracer(deblock_filter_optimize)]
 pub fn deblock_filter_optimize<T: Pixel, U: Pixel>(
   fi: &FrameInvariants<T>, rec: &Tile<U>, input: &Tile<U>,
   blocks: &TileBlocks, crop_w: usize, crop_h: usize,
--- a/src/encoder.rs
+++ b/src/encoder.rs
@@ -41,7 +41,6 @@
 use arg_enum_proc_macro::ArgEnum;
 use arrayvec::*;
 use bitstream_io::{BigEndian, BitWrite, BitWriter};
-use rust_hawktracer::*;
 
 use std::collections::VecDeque;
 use std::io::Write;
@@ -570,7 +569,6 @@
 }
 
 impl SegmentationState {
-  #[hawktracer(SegmentationState_update_threshold)]
   pub fn update_threshold(&mut self, base_q_idx: u8, bd: usize) {
     let base_ac_q = ac_q(base_q_idx, 0, bd).get() as u64;
     let real_ac_q = ArrayVec::<_, MAX_SEGMENTS>::from_iter(
@@ -748,7 +746,6 @@
 
   // Assumes that we have already computed activity scales and distortion scales
   // Returns -0.5 log2(mean(scale))
-  #[hawktracer(compute_spatiotemporal_scores)]
   pub fn compute_spatiotemporal_scores(&mut self) -> i64 {
     let mut scores = self
       .distortion_scales
@@ -774,7 +771,6 @@
 
   // Assumes that we have already computed distortion_scales
   // Returns -0.5 log2(mean(scale))
-  #[hawktracer(compute_temporal_scores)]
   pub fn compute_temporal_scores(&mut self) -> i64 {
     let inv_mean = DistortionScale::inv_mean(&self.distortion_scales);
     for scale in self.distortion_scales.iter_mut() {
@@ -1657,7 +1653,6 @@
 /// # Panics
 ///
 /// - If the block size is invalid for subsampling
-#[hawktracer(motion_compensate)]
 pub fn motion_compensate<T: Pixel>(
   fi: &FrameInvariants<T>, ts: &mut TileStateMut<'_, T>,
   cw: &mut ContextWriter, luma_mode: PredictionMode, ref_frames: [RefType; 2],
@@ -1886,7 +1881,6 @@
   }
 }
 
-#[hawktracer(encode_block_pre_cdef)]
 pub fn encode_block_pre_cdef<T: Pixel, W: Writer>(
   seq: &Sequence, ts: &TileStateMut<'_, T>, cw: &mut ContextWriter, w: &mut W,
   bsize: BlockSize, tile_bo: TileBlockOffset, skip: bool,
@@ -1927,7 +1921,6 @@
 ///
 /// - If chroma and luma do not match for inter modes
 /// - If an invalid motion vector is found
-#[hawktracer(encode_block_post_cdef)]
 pub fn encode_block_post_cdef<T: Pixel, W: Writer>(
   fi: &FrameInvariants<T>, ts: &mut TileStateMut<'_, T>,
   cw: &mut ContextWriter, w: &mut W, luma_mode: PredictionMode,
@@ -2552,7 +2545,6 @@
   (partition_has_coeff, tx_dist)
 }
 
-#[hawktracer(encode_block_with_modes)]
 pub fn encode_block_with_modes<T: Pixel, W: Writer>(
   fi: &FrameInvariants<T>, ts: &mut TileStateMut<'_, T>,
   cw: &mut ContextWriter, w_pre_cdef: &mut W, w_post_cdef: &mut W,
@@ -2619,7 +2611,6 @@
   );
 }
 
-#[hawktracer(encode_partition_bottomup)]
 fn encode_partition_bottomup<T: Pixel, W: Writer>(
   fi: &FrameInvariants<T>, ts: &mut TileStateMut<'_, T>,
   cw: &mut ContextWriter, w_pre_cdef: &mut W, w_post_cdef: &mut W,
@@ -3222,7 +3213,6 @@
   cdf.unwrap_or_else(|| CDFContext::new(fi.base_q_idx))
 }
 
-#[hawktracer(encode_tile_group)]
 fn encode_tile_group<T: Pixel>(
   fi: &FrameInvariants<T>, fs: &mut FrameState<T>, inter_cfg: &InterConfig,
 ) -> Vec<u8> {
@@ -3361,7 +3351,6 @@
   pub w_post_cdef: WriterBase<WriterRecorder>,
 }
 
-#[hawktracer(check_lf_queue)]
 fn check_lf_queue<T: Pixel>(
   fi: &FrameInvariants<T>, ts: &mut TileStateMut<'_, T>,
   cw: &mut ContextWriter, w: &mut WriterBase<WriterEncoder>,
@@ -3453,7 +3442,6 @@
   }
 }
 
-#[hawktracer(encode_tile)]
 fn encode_tile<'a, T: Pixel>(
   fi: &FrameInvariants<T>, ts: &'a mut TileStateMut<'_, T>,
   fc: &'a mut CDFContext, blocks: &'a mut TileBlocksMut<'a>,
@@ -3678,7 +3666,6 @@
 /// # Panics
 ///
 /// - If the frame packets cannot be written
-#[hawktracer(encode_show_existing_frame)]
 pub fn encode_show_existing_frame<T: Pixel>(
   fi: &FrameInvariants<T>, fs: &mut FrameState<T>, inter_cfg: &InterConfig,
 ) -> Vec<u8> {
@@ -3753,7 +3740,6 @@
 /// # Panics
 ///
 /// - If the frame packets cannot be written
-#[hawktracer(encode_frame)]
 pub fn encode_frame<T: Pixel>(
   fi: &FrameInvariants<T>, fs: &mut FrameState<T>, inter_cfg: &InterConfig,
 ) -> Vec<u8> {
--- a/src/lrf.rs
+++ b/src/lrf.rs
@@ -24,7 +24,6 @@
 };
 use crate::tiling::{Area, PlaneRegion, PlaneRegionMut, Rect};
 use crate::util::{clamp, CastFromPrimitive, ILog, Pixel};
-use rust_hawktracer::*;
 use std::cmp;
 use std::iter::FusedIterator;
 use std::ops::{Index, IndexMut};
@@ -526,7 +525,6 @@
 impl<T: Pixel> ExactSizeIterator for HorzPaddedIter<'_, T> {}
 impl<T: Pixel> FusedIterator for HorzPaddedIter<'_, T> {}
 
-#[hawktracer(setup_integral_image)]
 pub fn setup_integral_image<T: Pixel>(
   integral_image_buffer: &mut IntegralImageBuffer,
   integral_image_stride: usize, crop_w: usize, crop_h: usize, stripe_w: usize,
@@ -626,7 +624,6 @@
   }
 }
 
-#[hawktracer(sgrproj_stripe_filter)]
 pub fn sgrproj_stripe_filter<T: Pixel, U: Pixel>(
   set: u8, xqd: [i8; 2], fi: &FrameInvariants<T>,
   integral_image_buffer: &IntegralImageBuffer, integral_image_stride: usize,
@@ -843,7 +840,6 @@
 
 // Input params follow the same rules as sgrproj_stripe_filter.
 // Inputs are relative to the colocated slice views.
-#[hawktracer(sgrproj_solve)]
 pub fn sgrproj_solve<T: Pixel>(
   set: u8, fi: &FrameInvariants<T>,
   integral_image_buffer: &IntegralImageBuffer, input: &PlaneRegion<'_, T>,
@@ -1095,7 +1091,6 @@
   }
 }
 
-#[hawktracer(wiener_stripe_filter)]
 fn wiener_stripe_filter<T: Pixel>(
   coeffs: [[i8; 3]; 2], fi: &FrameInvariants<T>, crop_w: usize, crop_h: usize,
   stripe_w: usize, stripe_h: usize, stripe_x: usize, stripe_y: isize,
@@ -1484,7 +1479,6 @@
     }
   }
 
-  #[hawktracer(lrf_filter_frame)]
   pub fn lrf_filter_frame<T: Pixel>(
     &mut self, out: &mut Frame<T>, pre_cdef: &Frame<T>,
     fi: &FrameInvariants<T>,
--- a/src/me.rs
+++ b/src/me.rs
@@ -24,7 +24,6 @@
 use crate::FrameInvariants;
 
 use arrayvec::*;
-use rust_hawktracer::*;
 use std::ops::{Index, IndexMut};
 use std::sync::{Arc, RwLock, RwLockReadGuard, RwLockWriteGuard};
 
@@ -383,7 +382,6 @@
   }
 }
 
-#[hawktracer(get_subset_predictors)]
 fn get_subset_predictors(
   tile_bo: TileBlockOffset, tile_me_stats: &TileMEStats<'_>,
   frame_ref_opt: Option<ReadGuardMEStats<'_>>, ref_frame_id: usize,
@@ -690,7 +688,6 @@
   }
 }
 
-#[hawktracer(full_pixel_me)]
 fn full_pixel_me<T: Pixel>(
   fi: &FrameInvariants<T>, ts: &TileStateMut<'_, T>,
   org_region: &PlaneRegion<T>, p_ref: &Plane<T>, tile_bo: TileBlockOffset,
@@ -882,7 +879,6 @@
   );
 }
 
-#[hawktracer(get_best_predictor)]
 fn get_best_predictor<T: Pixel>(
   fi: &FrameInvariants<T>, po: PlaneOffset, org_region: &PlaneRegion<T>,
   p_ref: &Plane<T>, predictors: &[MotionVector], bit_depth: usize,
@@ -953,7 +949,6 @@
 /// For each step size, candidate motion vectors are examined for improvement
 /// to the current search location. The search location is moved to the best
 /// candidate (if any). This is repeated until the search location stops moving.
-#[hawktracer(fullpel_diamond_search)]
 fn fullpel_diamond_search<T: Pixel>(
   fi: &FrameInvariants<T>, po: PlaneOffset, org_region: &PlaneRegion<T>,
   p_ref: &Plane<T>, current: &mut MotionSearchResult, bit_depth: usize,
@@ -1053,7 +1048,6 @@
 ///
 /// `current` provides the initial search location and serves as
 /// the output for the final search results.
-#[hawktracer(hexagon_search)]
 fn hexagon_search<T: Pixel>(
   fi: &FrameInvariants<T>, po: PlaneOffset, org_region: &PlaneRegion<T>,
   p_ref: &Plane<T>, current: &mut MotionSearchResult, bit_depth: usize,
@@ -1168,7 +1162,6 @@
 /// the output for the final search results.
 ///
 /// `me_range` parameter determines how far these stages can search.
-#[hawktracer(uneven_multi_hex_search)]
 fn uneven_multi_hex_search<T: Pixel>(
   fi: &FrameInvariants<T>, po: PlaneOffset, org_region: &PlaneRegion<T>,
   p_ref: &Plane<T>, current: &mut MotionSearchResult, bit_depth: usize,
@@ -1309,7 +1302,6 @@
 /// For each step size, candidate motion vectors are examined for improvement
 /// to the current search location. The search location is moved to the best
 /// candidate (if any). This is repeated until the search location stops moving.
-#[hawktracer(subpel_diamond_search)]
 fn subpel_diamond_search<T: Pixel>(
   fi: &FrameInvariants<T>, po: PlaneOffset, org_region: &PlaneRegion<T>,
   _p_ref: &Plane<T>, bit_depth: usize, pmv: [MotionVector; 2], lambda: u32,
@@ -1462,7 +1454,6 @@
   MVCandidateRD { cost: 256 * sad as u64 + rate as u64 * lambda as u64, sad }
 }
 
-#[hawktracer(full_search)]
 fn full_search<T: Pixel>(
   fi: &FrameInvariants<T>, x_lo: isize, x_hi: isize, y_lo: isize, y_hi: isize,
   w: usize, h: usize, org_region: &PlaneRegion<T>, p_ref: &Plane<T>,
--- a/src/scenechange/mod.rs
+++ b/src/scenechange/mod.rs
@@ -17,7 +17,6 @@
 use crate::me::RefMEStats;
 use crate::util::Pixel;
 use debug_unreachable::debug_unreachable;
-use rust_hawktracer::*;
 use std::collections::BTreeMap;
 use std::sync::Arc;
 use std::{cmp, u64};
@@ -164,7 +163,6 @@
   /// to the second frame in `frame_set`.
   ///
   /// This will gracefully handle the first frame in the video as well.
-  #[hawktracer(analyze_next_frame)]
   pub fn analyze_next_frame(
     &mut self, frame_set: &[&Arc<Frame<T>>], input_frameno: u64,
     previous_keyframe: u64,
--- a/src/bin/muxer/y4m.rs
+++ b/src/bin/muxer/y4m.rs
@@ -9,11 +9,9 @@
 
 use crate::decoder::VideoDetails;
 use rav1e::prelude::*;
-use rust_hawktracer::*;
 use std::io::Write;
 use std::slice;
 
-#[hawktracer(write_y4m_frame)]
 pub fn write_y4m_frame<T: Pixel>(
   y4m_enc: &mut y4m::Encoder<Box<dyn Write + Send>>, rec: &Frame<T>,
   y4m_details: VideoDetails,
--- a/src/bin/rav1e.rs
+++ b/src/bin/rav1e.rs
@@ -54,7 +54,6 @@
 use crate::stats::*;
 use rav1e::config::CpuFeatureLevel;
 use rav1e::prelude::*;
-use rust_hawktracer::*;
 
 use crate::decoder::{Decoder, FrameBuilder, VideoDetails};
 use crate::muxer::*;
@@ -105,7 +104,6 @@
     }
   }
 
-  #[hawktracer(Source_read_frame)]
   fn read_frame<T: Pixel>(
     &mut self, ctx: &mut Context<T>, video_info: VideoDetails,
   ) -> Result<(), CliError> {
@@ -141,7 +139,6 @@
 
 // Encode and write a frame.
 // Returns frame information in a `Result`.
-#[hawktracer(process_frame)]
 fn process_frame<T: Pixel, D: Decoder>(
   ctx: &mut Context<T>, output_file: &mut dyn Muxer, source: &mut Source<D>,
   pass1file: Option<&mut File>, pass2file: Option<&mut File>,
--- a/src/context/block_unit.rs
+++ b/src/context/block_unit.rs
@@ -10,7 +10,6 @@
 use super::*;
 
 use crate::predict::PredictionMode;
-use rust_hawktracer::*;
 
 pub const MAX_PLANES: usize = 3;
 
@@ -1122,7 +1121,6 @@
     }
   }
 
-  #[hawktracer(setup_mvref_list)]
   fn setup_mvref_list<T: Pixel>(
     &self, bo: TileBlockOffset, ref_frames: [RefType; 2],
     mv_stack: &mut ArrayVec<CandidateMV, 9>, bsize: BlockSize,
--- a/src/rate.rs
+++ b/src/rate.rs
@@ -14,7 +14,6 @@
 use crate::util::{
   bexp64, bexp_q24, blog64, clamp, q24_to_q57, q57, q57_to_q24, Pixel,
 };
-use rust_hawktracer::*;
 use std::cmp;
 
 // The number of frame sub-types for which we track distinct parameters.
@@ -720,7 +719,6 @@
   }
 
   // TODO: Separate quantizers for Cb and Cr.
-  #[hawktracer(select_qi)]
   pub(crate) fn select_qi<T: Pixel>(
     &self, ctx: &ContextInner<T>, output_frameno: u64, fti: usize,
     maybe_prev_log_base_q: Option<i64>, log_isqrt_mean_scale: i64,
@@ -1069,7 +1067,6 @@
     (log_base_q, log_q)
   }
 
-  #[hawktracer(RCState_update_state)]
   pub fn update_state(
     &mut self, bits: i64, fti: usize, show_frame: bool, log_target_q: i64,
     trial: bool, droppable: bool,
--- a/src/rdo.rs
+++ b/src/rdo.rs
@@ -46,7 +46,6 @@
 
 use arrayvec::*;
 use itertools::izip;
-use rust_hawktracer::*;
 use std::fmt;
 use std::mem::MaybeUninit;
 
@@ -811,7 +810,6 @@
 }
 
 #[inline]
-#[hawktracer(luma_chroma_mode_rdo)]
 fn luma_chroma_mode_rdo<T: Pixel>(
   luma_mode: PredictionMode, fi: &FrameInvariants<T>, bsize: BlockSize,
   tile_bo: TileBlockOffset, ts: &mut TileStateMut<'_, T>,
@@ -958,7 +956,6 @@
 ///
 /// - If the best RD found is negative.
 ///   This should never happen and indicates a development error.
-#[hawktracer(rdo_mode_decision)]
 pub fn rdo_mode_decision<T: Pixel>(
   fi: &FrameInvariants<T>, ts: &mut TileStateMut<'_, T>,
   cw: &mut ContextWriter, bsize: BlockSize, tile_bo: TileBlockOffset,
@@ -1116,7 +1113,6 @@
   }
 }
 
-#[hawktracer(inter_frame_rdo_mode_decision)]
 fn inter_frame_rdo_mode_decision<T: Pixel>(
   fi: &FrameInvariants<T>, ts: &mut TileStateMut<'_, T>,
   cw: &mut ContextWriter, bsize: BlockSize, tile_bo: TileBlockOffset,
@@ -1389,7 +1385,6 @@
   best
 }
 
-#[hawktracer(intra_frame_rdo_mode_decision)]
 fn intra_frame_rdo_mode_decision<T: Pixel>(
   fi: &FrameInvariants<T>, ts: &mut TileStateMut<'_, T>,
   cw: &mut ContextWriter, bsize: BlockSize, tile_bo: TileBlockOffset,
@@ -1586,7 +1581,6 @@
 /// # Panics
 ///
 /// - If the block size is invalid for subsampling.
-#[hawktracer(rdo_cfl_alpha)]
 pub fn rdo_cfl_alpha<T: Pixel>(
   ts: &mut TileStateMut<'_, T>, tile_bo: TileBlockOffset, bsize: BlockSize,
   luma_tx_size: TxSize, fi: &FrameInvariants<T>,
@@ -1941,7 +1935,6 @@
 ///
 /// - If the best RD found is negative.
 ///   This should never happen, and indicates a development error.
-#[hawktracer(rdo_partition_decision)]
 pub fn rdo_partition_decision<T: Pixel, W: Writer>(
   fi: &FrameInvariants<T>, ts: &mut TileStateMut<'_, T>,
   cw: &mut ContextWriter, w_pre_cdef: &mut W, w_post_cdef: &mut W,
@@ -2019,7 +2012,6 @@
   }
 }
 
-#[hawktracer(rdo_loop_plane_error)]
 fn rdo_loop_plane_error<T: Pixel>(
   base_sbo: TileSuperBlockOffset, offset_sbo: TileSuperBlockOffset,
   sb_w: usize, sb_h: usize, fi: &FrameInvariants<T>, ts: &TileStateMut<'_, T>,
@@ -2096,7 +2088,6 @@
 /// # Panics
 ///
 /// - If both CDEF and LRF are disabled.
-#[hawktracer(rdo_loop_decision)]
 pub fn rdo_loop_decision<T: Pixel, W: Writer>(
   base_sbo: TileSuperBlockOffset, fi: &FrameInvariants<T>,
   ts: &mut TileStateMut<'_, T>, cw: &mut ContextWriter, w: &mut W,
--- a/src/scenechange/fast.rs
+++ b/src/scenechange/fast.rs
@@ -8,7 +8,6 @@
   scenechange::fast_idiv,
 };
 use debug_unreachable::debug_unreachable;
-use rust_hawktracer::*;
 use v_frame::pixel::Pixel;
 
 use super::{ScaleFunction, SceneChangeDetector, ScenecutResult};
@@ -19,7 +18,6 @@
 impl<T: Pixel> SceneChangeDetector<T> {
   /// The fast algorithm detects fast cuts using a raw difference
   /// in pixel values between the scaled frames.
-  #[hawktracer(fast_scenecut)]
   pub(super) fn fast_scenecut(
     &mut self, frame1: Arc<Frame<T>>, frame2: Arc<Frame<T>>,
   ) -> ScenecutResult {
@@ -104,7 +102,6 @@
   }
 
   /// Calculates the average sum of absolute difference (SAD) per pixel between 2 planes
-  #[hawktracer(delta_in_planes)]
   fn delta_in_planes(&self, plane1: &Plane<T>, plane2: &Plane<T>) -> f64 {
     let delta = sad_plane::sad_plane(plane1, plane2, self.cpu_feature_level);
 
--- a/src/segmentation.rs
+++ b/src/segmentation.rs
@@ -16,11 +16,9 @@
 use crate::util::Pixel;
 use crate::FrameInvariants;
 use crate::FrameState;
-use rust_hawktracer::*;
 
 pub const MAX_SEGMENTS: usize = 8;
 
-#[hawktracer(segmentation_optimize)]
 pub fn segmentation_optimize<T: Pixel>(
   fi: &FrameInvariants<T>, fs: &mut FrameState<T>,
 ) {
@@ -160,7 +158,6 @@
   fs.segmentation.update_threshold(fi.base_q_idx, fi.config.bit_depth);
 }
 
-#[hawktracer(select_segment)]
 pub fn select_segment<T: Pixel>(
   fi: &FrameInvariants<T>, ts: &TileStateMut<'_, T>, tile_bo: TileBlockOffset,
   bsize: BlockSize, skip: bool,
--- a/src/bin/rav1e-ch.rs
+++ b/src/bin/rav1e-ch.rs
@@ -314,18 +314,6 @@
 }
 
 fn main() -> Result<(), Box<dyn std::error::Error>> {
-  #[cfg(feature = "tracing")]
-  use rust_hawktracer::*;
-  init_logger();
-
-  #[cfg(feature = "tracing")]
-  let instance = HawktracerInstance::new();
-  #[cfg(feature = "tracing")]
-  let _listener = instance.create_listener(HawktracerListenerType::ToFile {
-    file_path: "trace.bin".into(),
-    buffer_size: 4096,
-  });
-
   run().map_err(|e| {
     error::print_error(&e);
     Box::new(e) as Box<dyn std::error::Error>
