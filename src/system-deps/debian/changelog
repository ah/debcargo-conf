rust-system-deps (6.1.1-1) unstable; urgency=medium

  * Package system-deps 6.1.1 from crates.io using debcargo 2.6.0
  * Drop cfg-expr-0.15 patch, upstream updated (Closes: #1040652)
  * Update testdata.patch with new test cases

  [ Jelmer Vernooĳ ]
  * Team upload.
  * Add patch cfg-expr-0.15: support building with newer cfg-expr.

 -- Blair Noctis <n@sail.ng>  Tue, 11 Jul 2023 18:43:59 +0000

rust-system-deps (6.0.2-2) unstable; urgency=medium

  * Team upload.
  * Package system-deps 6.0.2 from crates.io using debcargo 2.5.0
  * Add missing test data files to fix the autopkgtest.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 06 Oct 2022 19:36:17 +0000

rust-system-deps (6.0.2-1) unstable; urgency=medium

  * Team upload.
  * Package system-deps 6.0.2 from crates.io using debcargo 2.5.0

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sun, 02 Oct 2022 21:10:23 +0200

rust-system-deps (3.2.0-2) unstable; urgency=medium

  * Team upload.
  * Package system-deps 3.2.0 from crates.io using debcargo 2.5.0
  * Refresh patches to disable tests requiring assert_matches and add new test
    data

 -- Sebastian Ramacher <sramacher@debian.org>  Mon, 11 Jul 2022 23:04:19 +0200

rust-system-deps (3.2.0-1) unstable; urgency=medium

  * Team upload.
  * Package system-deps 3.2.0 from crates.io using debcargo 2.5.0

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 09 Jul 2022 14:18:27 +0200

rust-system-deps (3.0.0-3) unstable; urgency=medium

  * Team upload.
  * Package system-deps 3.0.0 from crates.io using debcargo 2.5.0
  * Fix patch to disable assert_matches so the tests actually pass.
  * Add test data from upstream git.
  * Don't fail the tests if the pkg-config crate returns an unexpected
    error type. Version 0.3.23 added a new error type. Upstream has
    a proper fix but upstreams fix breaks compatibility with older
    versions of the pkg-config crate. This seems like the least bad
    way to support both.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 14 Apr 2022 19:53:02 +0000

rust-system-deps (3.0.0-2) unstable; urgency=medium

  * Team upload.
  * Package system-deps 3.0.0 from crates.io using debcargo 2.5.0
  * Add patch for heck 0.4.
  * Disable tests that use assert_matches and remove the dev dependency
    so the rest of the testsuite can run.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 14 Apr 2022 08:43:19 +0000

rust-system-deps (3.0.0-1) unstable; urgency=medium

  * Team upload.
  * Package system-deps 3.0.0 from crates.io using debcargo 2.4.4
  * d/patches: removed patch relax-deps.patch

 -- Henry-Nicolas Tourneur <debian@nilux.be>  Sun, 28 Nov 2021 21:42:25 +0100

rust-system-deps (1.3.2-2) unstable; urgency=medium

  [ Sylvestre Ledru ]
  * Package system-deps 1.3.2 from crates.io using debcargo 2.4.3
  * Source upload

  [ Henry-Nicolas Tourneur ]
  * d/patches: relax-deps, match the version of strum availabe in the
    archive.

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 31 Oct 2020 21:54:22 +0100

rust-system-deps (1.3.2-1) unstable; urgency=medium

  * Package system-deps 1.3.2 from crates.io using debcargo 2.4.3

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 27 Sep 2020 21:30:35 +0200
