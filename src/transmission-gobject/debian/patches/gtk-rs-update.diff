Description: Updated transmission-goject to use gtk-rs 0.16
Author: Matthias Geiger <matthias.geiger1024@tutanota.de>
Forwarded: https://gitlab.gnome.org/haecker-felix/transmission-gobject/-/issues/2
Last-Update: 2023-03-19
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
diff --git a/Cargo.toml b/Cargo.toml
index af716d3..a7c57d3 100644
--- a/Cargo.toml
+++ b/Cargo.toml
@@ -27,10 +27,10 @@ version = "1.6"
 version = "1.10"
 
 [dependencies.gio]
-version = "0.15"
+version = "0.16"
 
 [dependencies.glib]
-version = "0.15"
+version = "0.16"
 
 [dependencies.gtk-macros]
 version = "0.3"
diff --git a/src/authentication.rs b/src/authentication.rs
index c5203ab..8ec026d 100644
--- a/src/authentication.rs
+++ b/src/authentication.rs
@@ -43,7 +43,7 @@ mod imp {
             PROPERTIES.as_ref()
         }
 
-        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> glib::Value {
+        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
             match pspec.name() {
                 "username" => self.username.get().to_value(),
                 "password" => self.password.get().to_value(),
@@ -53,7 +53,6 @@ mod imp {
 
         fn set_property(
             &self,
-            _obj: &Self::Type,
             _id: usize,
             value: &glib::Value,
             pspec: &ParamSpec,
@@ -73,7 +72,7 @@ glib::wrapper! {
 
 impl TrAuthentication {
     pub fn new(username: &str, password: &str) -> Self {
-        glib::Object::new(&[("username", &username), ("password", &password)]).unwrap()
+        glib::Object::new(&[("username", &username), ("password", &password)])
     }
 
     pub fn username(&self) -> String {
diff --git a/src/client.rs b/src/client.rs
index fd423a8..28fae4a 100644
--- a/src/client.rs
+++ b/src/client.rs
@@ -69,19 +69,9 @@ mod imp {
         fn signals() -> &'static [Signal] {
             static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
                 vec![
-                    Signal::builder("connection-failure", &[], glib::Type::UNIT.into()).build(),
-                    Signal::builder(
-                        "torrent-added",
-                        &[TrTorrent::static_type().into()],
-                        glib::Type::UNIT.into(),
-                    )
-                    .build(),
-                    Signal::builder(
-                        "torrent-downloaded",
-                        &[TrTorrent::static_type().into()],
-                        glib::Type::UNIT.into(),
-                    )
-                    .build(),
+                    Signal::builder("connection-failure").build(),
+                    Signal::builder("torrent-added").build(),
+                    Signal::builder("torrent-downloaded").build(),
                 ]
             });
             SIGNALS.as_ref()
@@ -146,7 +136,7 @@ mod imp {
             PROPERTIES.as_ref()
         }
 
-        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> glib::Value {
+        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
             match pspec.name() {
                 "address" => self.address.borrow().to_value(),
                 "polling-rate" => self.polling_rate.borrow().to_value(),
@@ -167,7 +157,7 @@ glib::wrapper! {
 
 impl TrClient {
     pub fn new() -> Self {
-        let client: Self = glib::Object::new::<Self>(&[]).unwrap();
+        let client: Self = glib::Object::new::<Self>(&[]);
 
         let session = TrSession::new(&client);
         let imp = imp::TrClient::from_instance(&client);
diff --git a/src/session.rs b/src/session.rs
index 0abf598..baa6566 100644
--- a/src/session.rs
+++ b/src/session.rs
@@ -169,7 +169,7 @@ mod imp {
             PROPERTIES.as_ref()
         }
 
-        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> glib::Value {
+        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
             match pspec.name() {
                 "version" => self.version.borrow().to_value(),
                 "download-dir" => self.download_dir.borrow().to_value(),
@@ -192,11 +192,11 @@ mod imp {
 
         fn set_property(
             &self,
-            obj: &Self::Type,
             _id: usize,
             value: &glib::Value,
             pspec: &ParamSpec,
         ) {
+            let obj = self.obj();
             match pspec.name() {
                 "download-dir" => obj.set_download_dir(value.get().unwrap()),
                 "start-added-torrents" => obj.set_start_added_torrents(value.get().unwrap()),
@@ -226,7 +226,7 @@ glib::wrapper! {
 
 impl TrSession {
     pub(crate) fn new(client: &TrClient) -> Self {
-        let session: Self = glib::Object::new(&[]).unwrap();
+        let session: Self = glib::Object::new(&[]);
 
         let imp = imp::TrSession::from_instance(&session);
         imp.client.set(client.clone()).unwrap();
diff --git a/src/session_stats.rs b/src/session_stats.rs
index 43639f7..acd82ff 100644
--- a/src/session_stats.rs
+++ b/src/session_stats.rs
@@ -108,7 +108,7 @@ mod imp {
             PROPERTIES.as_ref()
         }
 
-        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> glib::Value {
+        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
             match pspec.name() {
                 "torrent-count" => self.torrent_count.get().to_value(),
                 "active-torrent-count" => self.active_torrent_count.get().to_value(),
@@ -208,6 +208,6 @@ impl TrSessionStats {
 
 impl Default for TrSessionStats {
     fn default() -> Self {
-        glib::Object::new(&[]).unwrap()
+        glib::Object::new(&[])
     }
 }
diff --git a/src/session_stats_details.rs b/src/session_stats_details.rs
index e7fc416..82c09a4 100644
--- a/src/session_stats_details.rs
+++ b/src/session_stats_details.rs
@@ -79,7 +79,7 @@ mod imp {
             PROPERTIES.as_ref()
         }
 
-        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> glib::Value {
+        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
             match pspec.name() {
                 "seconds-active" => self.seconds_active.get().to_value(),
                 "downloaded-bytes" => self.downloaded_bytes.get().to_value(),
@@ -154,6 +154,6 @@ impl TrSessionStatsDetails {
 
 impl Default for TrSessionStatsDetails {
     fn default() -> Self {
-        glib::Object::new(&[]).unwrap()
+        glib::Object::new(&[])
     }
 }
diff --git a/src/torrent.rs b/src/torrent.rs
index 1415750..0909078 100644
--- a/src/torrent.rs
+++ b/src/torrent.rs
@@ -241,7 +241,7 @@ mod imp {
             PROPERTIES.as_ref()
         }
 
-        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> glib::Value {
+        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
             match pspec.name() {
                 "name" => self.name.get().unwrap().to_value(),
                 "hash" => self.hash.get().unwrap().to_value(),
@@ -282,7 +282,7 @@ impl TrTorrent {
         seed_queue_pos: i32,
         client: TrClient,
     ) -> Self {
-        let torrent: Self = glib::Object::new(&[]).unwrap();
+        let torrent: Self = glib::Object::new(&[]);
         let imp = imp::TrTorrent::from_instance(&torrent);
 
         imp.client.set(client).unwrap();
diff --git a/src/torrent_model.rs b/src/torrent_model.rs
index 4b611c3..5d43701 100644
--- a/src/torrent_model.rs
+++ b/src/torrent_model.rs
@@ -26,9 +26,7 @@ mod imp {
         fn signals() -> &'static [Signal] {
             static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
                 vec![
-                    Signal::builder("status-changed", &[], glib::Type::UNIT.into())
-                        .flags(glib::SignalFlags::ACTION)
-                        .build(),
+                    Signal::builder("status-changed").build(),
                 ]
             });
             SIGNALS.as_ref()
@@ -36,15 +34,15 @@ mod imp {
     }
 
     impl ListModelImpl for TrTorrentModel {
-        fn item_type(&self, _list_model: &Self::Type) -> glib::Type {
+        fn item_type(&self) -> glib::Type {
             TrTorrent::static_type()
         }
 
-        fn n_items(&self, _list_model: &Self::Type) -> u32 {
+        fn n_items(&self) -> u32 {
             self.vec.borrow().len() as u32
         }
 
-        fn item(&self, _list_model: &Self::Type, position: u32) -> Option<glib::Object> {
+        fn item(&self, position: u32) -> Option<glib::Object> {
             self.vec
                 .borrow()
                 .get(position as usize)
@@ -128,6 +126,6 @@ impl TrTorrentModel {
 
 impl Default for TrTorrentModel {
     fn default() -> Self {
-        glib::Object::new(&[]).unwrap()
+        glib::Object::new(&[])
     }
 }
