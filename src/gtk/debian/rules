#!/usr/bin/make -f

%:
	dh $@ --buildsystem cargo

# regenerating the source code
# the xmlstarlet fixes are taken from upstream here: https://github.com/gtk-rs/gir-files/blob/master/fix.sh
execute_before_dh_auto_build:
	cp /usr/share/gir-1.0/GLib-2.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/GObject-2.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/Pango-1.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/Gio-2.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/HarfBuzz-0.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/freetype2-2.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/PangoCairo-1.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/Gtk-3.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/cairo-1.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/xlib-2.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/Atk-1.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/Gdk-3.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/GdkPixbuf-2.0.gir $(CURDIR)
	cp /usr/share/gir-1.0/GModule-2.0.gir $(CURDIR)
	xmlstarlet ed -L \
	-d '///_:method[@c:identifier="atk_plug_set_child"]' \
	Atk-1.0.gir
	xmlstarlet ed -L \
	-d '///_:alias[@name="Int32"]' \
	freetype2-2.0.gir
	xmlstarlet ed -L \
	-u '//*[@glib:error-domain="g-option-context-error-quark"]/@glib:error-domain' -v g-option-error-quark \
	-u '//_:record[@name="KeyFile"]/_:method[@name="set_boolean_list"]//_:parameter[@name="list"]/_:array/@c:type' -v "gboolean*" \
	-u '//_:record[@name="KeyFile"]/_:method[@name="set_double_list"]//_:parameter[@name="list"]/_:array/@c:type' -v "gdouble*" \
	-u '//_:record[@name="KeyFile"]/_:method[@name="set_integer_list"]//_:parameter[@name="list"]/_:array/@c:type' -v "gint*" \
	-u '//_:record[@name="KeyFile"]/_:method[@name="set_locale_string_list"]//_:parameter[@name="list"]/_:array/@c:type' -v "const gchar* const*" \
	-u '//_:record[@name="KeyFile"]/_:method[@name="set_string_list"]//_:parameter[@name="list"]/_:array/@c:type' -v "const gchar* const*" \
	GLib-2.0.gir
	xmlstarlet ed -L \
	-u '//_:class[@name="Object"]/_:method[@name="getv"]//_:parameter[@name="names"]/_:array/@c:type' -v "const gchar**" \
	-u '//_:class[@name="Object"]/_:method[@name="getv"]//_:parameter[@name="values"]/_:array/@c:type' -v "GValue*" \
	-u '//_:class[@name="Object"]/_:method[@name="setv"]//_:parameter[@name="names"]/_:array/@c:type' -v "const gchar**" \
	-u '//_:class[@name="Object"]/_:method[@name="setv"]//_:parameter[@name="values"]/_:array/@c:type' -v "const GValue*" \
	-u '//_:class[@name="Object"]/_:constructor[@name="new_with_properties"]//_:parameter[@name="names"]/_:array/@c:type' -v "const char**" \
	-u '//_:class[@name="Object"]/_:constructor[@name="new_with_properties"]//_:parameter[@name="values"]/_:array/@c:type' -v "const GValue*" \
	-i '//_:interface[@name="TypePlugin" and not(@glib:type-struct)]' -t 'attr' -n 'glib:type-struct' -v 'TypePluginClass' \
	-i '//_:record[@name="TypePluginClass" and not(@glib:is-gtype-struct-for)]' -t 'attr' -n 'glib:is-gtype-struct-for' -v 'TypePlugin' \
	GObject-2.0.gir
	xmlstarlet ed -L \
	-i '///_:type[not(@name) and @c:type="hb_font_t*"]' -t 'attr' -n 'name' -v "gconstpointer" \
	-u '//_:type[@c:type="hb_font_t*"]/@c:type' -v "gconstpointer" \
	-i '///_:array[not(@name) and @c:type="hb_feature_t*"]' -t 'attr' -n 'name' -v "gconstpointer" \
	-r '///_:array[@c:type="hb_feature_t*"]' -v "type" \
	-d '//_:type[@c:type="hb_feature_t*"]/*' \
	-d '//_:type[@c:type="hb_feature_t*"]/@length' \
	-d '//_:type[@c:type="hb_feature_t*"]/@zero-terminated' \
	-u '//_:type[@c:type="hb_feature_t*"]/@c:type' -v "gconstpointer" \
	-d '//_:record[@c:type="PangoGlyphVisAttr"]/_:field/@bits' \
	-d '//_:record[@c:type="PangoGlyphVisAttr"]/_:field[@name="is_color"]' \
	Pango-1.0.gir
	xmlstarlet ed -L \
	-d '///_:function[@c:identifier="hb_graphite2_face_get_gr_face"]' \
	-d '///_:function[@c:identifier="hb_graphite2_font_get_gr_font"]' \
	-d '///_:function[@c:identifier="hb_ft_face_create"]' \
	-d '///_:function[@c:identifier="hb_ft_face_create_cached"]' \
	-d '///_:function[@c:identifier="hb_ft_face_create_referenced"]' \
	-d '///_:function[@c:identifier="hb_ft_font_create"]' \
	-d '///_:function[@c:identifier="hb_ft_font_create_cached"]' \
	-d '///_:function[@c:identifier="hb_ft_font_create_referenced"]' \
	-d '///_:function[@c:identifier="hb_ft_font_get_face"]' \
	-d '///_:function[@c:identifier="hb_ft_font_lock_face"]' \
	HarfBuzz-0.0.gir
	xmlstarlet ed -L \
	-u '//_:method[@c:identifier="gdk_frame_clock_get_current_timings"]/_:return-value/@transfer-ownership' -v "none" \
	-u '//_:method[@c:identifier="gdk_frame_clock_get_timings"]/_:return-value/@transfer-ownership' -v "none" \
	Gdk-3.0.gir
	xmlstarlet ed -L \
	-u '//_:record[@name="PixbufModule"]/_:field[@name="module"]/_:type/@name' -v "gpointer" \
	-u '//_:record[@name="PixbufModule"]/_:field[@name="module"]/_:type/@c:type' -v "gpointer" \
	GdkPixbuf-2.0.gir
	xmlstarlet ed -L \
	-u '//_:class[@name="Image"]/_:property[@name="icon-size"]/_:type/@c:type' -v "GtkIconSize" \
	-u '//_:class[@name="Image"]/_:property[@name="icon-size"]/_:type/@name' -v "IconSize" \
	-u '//_:class[@name="StackSwitcher"]/_:property[@name="icon-size"]/_:type/@c:type' -v "GtkIconSize" \
	-u '//_:class[@name="StackSwitcher"]/_:property[@name="icon-size"]/_:type/@name' -v "IconSize" \
	-u '//_:parameter[@name="response_id"]/_:type[@name="gint"]/@c:type' -v "GtkResponseType" \
	-u '//_:parameter[@name="response_id"]/_:type[@name="gint"]/@name' -v "ResponseType" \
	-u '//_:constructor[@c:identifier="gtk_shortcut_label_new"]/_:return-value/@transfer-ownership' -v "none" \
	-a '//_:method[@c:identifier="gtk_style_context_get_style_property"]//_:parameter[@name="value" and not(@direction)]' -type attr -n "direction" -v "out" \
	-a '//_:method[@c:identifier="gtk_style_context_get_style_property"]//_:parameter[@name="value" and not(@caller-allocates)]' -type attr -n "caller-allocates" -v "1" \
	-a '//_:method[@c:identifier="gtk_cell_area_cell_get_property"]//_:parameter[@name="value" and not(@direction)]' -type attr -n "direction" -v "out" \
	-a '//_:method[@c:identifier="gtk_cell_area_cell_get_property"]//_:parameter[@name="value" and not(@caller-allocates)]' -type attr -n "caller-allocates" -v "1" \
	-a '//_:method[@c:identifier="gtk_container_child_get_property"]//_:parameter[@name="value" and not(@direction)]' -type attr -n "direction" -v "out" \
	-a '//_:method[@c:identifier="gtk_container_child_get_property"]//_:parameter[@name="value" and not(@caller-allocates)]' -type attr -n "caller-allocates" -v "1" \
	-a '//_:method[@c:identifier="gtk_widget_style_get_property"]//_:parameter[@name="value" and not(@direction)]' -type attr -n "direction" -v "out" \
	-a '//_:method[@c:identifier="gtk_widget_style_get_property"]//_:parameter[@name="value" and not(@caller-allocates)]' -type attr -n "caller-allocates" -v "1" \
	-u '//_:class[@name="Entry"]/glib:signal[@name="icon-press"]//_:parameter[@name="event"]/_:type[@name="Gdk.EventButton"]/@name' -v "Gdk.Event" \
	-u '//_:class[@name="Entry"]/glib:signal[@name="icon-release"]//_:parameter[@name="event"]/_:type[@name="Gdk.EventButton"]/@name' -v "Gdk.Event" \
	-u '//_:type[@c:type="GtkIconSize"]/@name' -v "IconSize" \
	-u '//_:type[@c:type="GtkIconSize*"]/@name' -v "IconSize" \
	-u '//_:class[@name="IconTheme"]/_:method//_:parameter[@name="icon_names"]/_:array/@c:type' -v "const gchar**" \
	-u '//_:class[@name="IconTheme"]/_:method[@name="get_search_path"]//_:parameter[@name="path"]/_:array/@c:type' -v "gchar***" \
	-u '//_:class[@name="IconTheme"]/_:method[@name="set_search_path"]//_:parameter[@name="path"]/_:array/@c:type' -v "const gchar**" \
	Gtk-3.0.gir
	sed -i 's/girs_directories\s=\s\[\"\.\.\/gir-files\"\]/girs_directories=\[\".\"\]/' $(CURDIR)/Gir.toml
	gir -o .

# Remove the .gir file before install
execute_before_dh_auto_install:
	rm $(CURDIR)/*.gir

