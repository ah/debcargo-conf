diff --git a/src/backends/blt.rs b/src/backends/blt.rs
index e9d719e..22dfca7 100644
--- a/src/backends/blt.rs
+++ b/src/backends/blt.rs
@@ -2,7 +2,6 @@
 //!
 //! Requires the `blt-backend` feature.
 #![cfg(feature = "bear-lib-terminal")]
-#![cfg_attr(feature = "doc-cfg", doc(cfg(feature = "blt-backend")))]
 
 pub use bear_lib_terminal;
 
diff --git a/src/backends/crossterm.rs b/src/backends/crossterm.rs
index 4fb301c..2c9843a 100644
--- a/src/backends/crossterm.rs
+++ b/src/backends/crossterm.rs
@@ -2,7 +2,6 @@
 //!
 //! Requires the `crossterm-backend` feature.
 #![cfg(feature = "crossterm")]
-#![cfg_attr(feature = "doc-cfg", doc(cfg(feature = "crossterm-backend")))]
 
 use std::{
     cell::{Cell, RefCell, RefMut},
diff --git a/src/backends/curses/n.rs b/src/backends/curses/n.rs
index 583015d..2e951ed 100644
--- a/src/backends/curses/n.rs
+++ b/src/backends/curses/n.rs
@@ -1,6 +1,5 @@
 //! Ncurses-specific backend.
 #![cfg(feature = "ncurses-backend")]
-#![cfg_attr(feature = "doc-cfg", doc(cfg(feature = "ncurses-backend")))]
 pub use ncurses;
 
 use log::{debug, warn};
diff --git a/src/backends/curses/pan.rs b/src/backends/curses/pan.rs
index 74d080f..4dc0e56 100644
--- a/src/backends/curses/pan.rs
+++ b/src/backends/curses/pan.rs
@@ -1,6 +1,5 @@
 //! Pancuses-specific backend.
 #![cfg(feature = "pancurses-backend")]
-#![cfg_attr(feature = "doc-cfg", doc(cfg(feature = "pancurses-backend")))]
 
 pub use pancurses;
 
diff --git a/src/backends/termion.rs b/src/backends/termion.rs
index 05c1946..685bc4f 100644
--- a/src/backends/termion.rs
+++ b/src/backends/termion.rs
@@ -2,7 +2,6 @@
 //!
 //! Requires the `termion-backend` feature.
 #![cfg(feature = "termion")]
-#![cfg_attr(feature = "doc-cfg", doc(cfg(feature = "termion-backend")))]
 
 pub use termion;
 
diff --git a/src/cursive_ext.rs b/src/cursive_ext.rs
index 516357c..74b8bf0 100644
--- a/src/cursive_ext.rs
+++ b/src/cursive_ext.rs
@@ -37,27 +37,22 @@ pub trait CursiveExt {
 
     /// Creates a new Cursive root using a ncurses backend.
     #[cfg(feature = "ncurses-backend")]
-    #[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "ncurses-backend")))]
     fn run_ncurses(&mut self) -> std::io::Result<()>;
 
     /// Creates a new Cursive root using a pancurses backend.
     #[cfg(feature = "pancurses-backend")]
-    #[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "pancurses-backend")))]
     fn run_pancurses(&mut self) -> std::io::Result<()>;
 
     /// Creates a new Cursive root using a termion backend.
     #[cfg(feature = "termion-backend")]
-    #[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "termion-backend")))]
     fn run_termion(&mut self) -> std::io::Result<()>;
 
     /// Creates a new Cursive root using a crossterm backend.
     #[cfg(feature = "crossterm-backend")]
-    #[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "crossterm-backend")))]
     fn run_crossterm(&mut self) -> Result<(), crossterm::ErrorKind>;
 
     /// Creates a new Cursive root using a bear-lib-terminal backend.
     #[cfg(feature = "blt-backend")]
-    #[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "blt-backend")))]
     fn run_blt(&mut self);
 }
 
@@ -82,31 +77,26 @@ impl CursiveExt for cursive_core::Cursive {
     }
 
     #[cfg(feature = "ncurses-backend")]
-    #[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "curses-backend")))]
     fn run_ncurses(&mut self) -> std::io::Result<()> {
         self.try_run_with(crate::backends::curses::n::Backend::init)
     }
 
     #[cfg(feature = "pancurses-backend")]
-    #[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "pancurses-backend")))]
     fn run_pancurses(&mut self) -> std::io::Result<()> {
         self.try_run_with(crate::backends::curses::pan::Backend::init)
     }
 
     #[cfg(feature = "termion-backend")]
-    #[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "termion-backend")))]
     fn run_termion(&mut self) -> std::io::Result<()> {
         self.try_run_with(crate::backends::termion::Backend::init)
     }
 
     #[cfg(feature = "crossterm-backend")]
-    #[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "crossterm-backend")))]
     fn run_crossterm(&mut self) -> Result<(), crossterm::ErrorKind> {
         self.try_run_with(crate::backends::crossterm::Backend::init)
     }
 
     #[cfg(feature = "blt-backend")]
-    #[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "blt-backend")))]
     fn run_blt(&mut self) {
         self.run_with(crate::backends::blt::Backend::init)
     }
diff --git a/src/cursive_runnable.rs b/src/cursive_runnable.rs
index 52a975b..1912ab5 100644
--- a/src/cursive_runnable.rs
+++ b/src/cursive_runnable.rs
@@ -145,7 +145,6 @@ impl CursiveRunnable {
     ///
     /// _Requires the `ncurses-backend` feature._
     #[cfg(feature = "ncurses-backend")]
-    #[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "ncurses-backend")))]
     pub fn ncurses() -> Self {
         Self::new(backends::curses::n::Backend::init)
     }
@@ -154,7 +153,6 @@ impl CursiveRunnable {
     ///
     /// _Requires the `panncurses-backend` feature._
     #[cfg(feature = "pancurses-backend")]
-    #[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "pancurses-backend")))]
     pub fn pancurses() -> Self {
         Self::new(backends::curses::pan::Backend::init)
     }
@@ -163,7 +161,6 @@ impl CursiveRunnable {
     ///
     /// _Requires the `termion-backend` feature._
     #[cfg(feature = "termion-backend")]
-    #[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "termion-backend")))]
     pub fn termion() -> Self {
         Self::new(backends::termion::Backend::init)
     }
@@ -172,7 +169,6 @@ impl CursiveRunnable {
     ///
     /// _Requires the `crossterm-backend` feature._
     #[cfg(feature = "crossterm-backend")]
-    #[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "crossterm-backend")))]
     pub fn crossterm() -> Self {
         Self::new(backends::crossterm::Backend::init)
     }
@@ -181,7 +177,6 @@ impl CursiveRunnable {
     ///
     /// _Requires the `blt-backend` feature._
     #[cfg(feature = "blt-backend")]
-    #[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "blt-backend")))]
     pub fn blt() -> Self {
         Self::new::<std::convert::Infallible, _>(|| {
             Ok(backends::blt::Backend::init())
diff --git a/src/lib.rs b/src/lib.rs
index 939e4f8..8d12d3a 100644
--- a/src/lib.rs
+++ b/src/lib.rs
@@ -63,7 +63,6 @@
 //!
 //! [`cursive::theme`]: ./theme/index.html
 #![deny(missing_docs)]
-#![cfg_attr(feature = "doc-cfg", feature(doc_cfg))]
 
 pub use cursive_core::*;
 
@@ -97,35 +96,30 @@ pub fn default() -> CursiveRunnable {
 
 /// Creates a new Cursive root using a ncurses backend.
 #[cfg(feature = "ncurses-backend")]
-#[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "ncurses-backend")))]
 pub fn ncurses() -> CursiveRunnable {
     CursiveRunnable::ncurses()
 }
 
 /// Creates a new Cursive root using a pancurses backend.
 #[cfg(feature = "pancurses-backend")]
-#[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "pancurses-backend")))]
 pub fn pancurses() -> CursiveRunnable {
     CursiveRunnable::pancurses()
 }
 
 /// Creates a new Cursive root using a termion backend.
 #[cfg(feature = "termion-backend")]
-#[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "termion-backend")))]
 pub fn termion() -> CursiveRunnable {
     CursiveRunnable::termion()
 }
 
 /// Creates a new Cursive root using a crossterm backend.
 #[cfg(feature = "crossterm-backend")]
-#[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "crossterm-backend")))]
 pub fn crossterm() -> CursiveRunnable {
     CursiveRunnable::crossterm()
 }
 
 /// Creates a new Cursive root using a bear-lib-terminal backend.
 #[cfg(feature = "blt-backend")]
-#[cfg_attr(feature = "doc-cfg", doc(cfg(feature = "blt-backend")))]
 pub fn blt() -> CursiveRunnable {
     CursiveRunnable::blt()
 }
