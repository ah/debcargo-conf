This patch is based on the upstream commit described below, adapted for use
in the Debian package by Peter Michael Green.

commit 6007f05a85385fddbd525b4ad265bf1372fe6015
Author: Ed Page <eopage@gmail.com>
Date:   Thu Jan 19 15:26:28 2023 -0600

    chore: Update to toml v0.6, toml_edit v0.18
    
    `toml` replaces `toml_edit::easy`, using `toml_edit` as its parser.

Index: cargo/src/cargo/core/manifest.rs
===================================================================
--- cargo.orig/src/cargo/core/manifest.rs
+++ cargo/src/cargo/core/manifest.rs
@@ -9,7 +9,6 @@ use anyhow::Context as _;
 use semver::Version;
 use serde::ser;
 use serde::Serialize;
-use toml_edit::easy as toml;
 use url::Url;
 
 use crate::core::compiler::{CompileKind, CrateType};
Index: cargo/src/cargo/core/package.rs
===================================================================
--- cargo.orig/src/cargo/core/package.rs
+++ cargo/src/cargo/core/package.rs
@@ -16,7 +16,6 @@ use lazycell::LazyCell;
 use log::{debug, warn};
 use semver::Version;
 use serde::Serialize;
-use toml_edit::easy as toml;
 
 use crate::core::compiler::{CompileKind, RustcTargetData};
 use crate::core::dependency::DepKind;
Index: cargo/src/cargo/core/workspace.rs
===================================================================
--- cargo.orig/src/cargo/core/workspace.rs
+++ cargo/src/cargo/core/workspace.rs
@@ -8,7 +8,6 @@ use anyhow::{anyhow, bail, Context as _}
 use glob::glob;
 use itertools::Itertools;
 use log::debug;
-use toml_edit::easy as toml;
 use url::Url;
 
 use crate::core::compiler::Unit;
Index: cargo/src/cargo/ops/cargo_config.rs
===================================================================
--- cargo.orig/src/cargo/ops/cargo_config.rs
+++ cargo/src/cargo/ops/cargo_config.rs
@@ -137,7 +137,8 @@ fn print_toml(config: &Config, opts: &Ge
                     drop_println!(
                         config,
                         "    {}, # {}",
-                        toml_edit::ser::to_item(&val).unwrap(),
+                        serde::Serialize::serialize(val, toml_edit::ser::ValueSerializer::new())
+                            .unwrap(),
                         def
                     );
                 }
Index: cargo/src/cargo/ops/cargo_new.rs
===================================================================
--- cargo.orig/src/cargo/ops/cargo_new.rs
+++ cargo/src/cargo/ops/cargo_new.rs
@@ -11,7 +11,6 @@ use std::fmt;
 use std::io::{BufRead, BufReader, ErrorKind};
 use std::path::{Path, PathBuf};
 use std::str::FromStr;
-use toml_edit::easy as toml;
 
 #[derive(Clone, Copy, Debug, PartialEq)]
 pub enum VersionControl {
Index: cargo/src/cargo/ops/cargo_output_metadata.rs
===================================================================
--- cargo.orig/src/cargo/ops/cargo_output_metadata.rs
+++ cargo/src/cargo/ops/cargo_output_metadata.rs
@@ -10,7 +10,6 @@ use cargo_platform::Platform;
 use serde::Serialize;
 use std::collections::BTreeMap;
 use std::path::PathBuf;
-use toml_edit::easy as toml;
 
 const VERSION: u32 = 1;
 
Index: cargo/src/cargo/ops/common_for_install_and_uninstall.rs
===================================================================
--- cargo.orig/src/cargo/ops/common_for_install_and_uninstall.rs
+++ cargo/src/cargo/ops/common_for_install_and_uninstall.rs
@@ -8,7 +8,6 @@ use std::task::Poll;
 
 use anyhow::{bail, format_err, Context as _};
 use serde::{Deserialize, Serialize};
-use toml_edit::easy as toml;
 
 use crate::core::compiler::Freshness;
 use crate::core::{Dependency, FeatureValue, Package, PackageId, QueryKind, Source, SourceId};
Index: cargo/src/cargo/ops/lockfile.rs
===================================================================
--- cargo.orig/src/cargo/ops/lockfile.rs
+++ cargo/src/cargo/ops/lockfile.rs
@@ -6,7 +6,6 @@ use crate::util::toml as cargo_toml;
 use crate::util::Filesystem;
 
 use anyhow::Context as _;
-use toml_edit::easy as toml;
 
 pub fn load_pkg_lockfile(ws: &Workspace<'_>) -> CargoResult<Option<Resolve>> {
     if !ws.root().join("Cargo.lock").exists() {
@@ -21,7 +20,7 @@ pub fn load_pkg_lockfile(ws: &Workspace<
         .with_context(|| format!("failed to read file: {}", f.path().display()))?;
 
     let resolve = (|| -> CargoResult<Option<Resolve>> {
-        let resolve: toml::Value = cargo_toml::parse(&s, f.path(), ws.config())?;
+        let resolve: toml::Table = cargo_toml::parse_document(&s, f.path(), ws.config())?;
         let v: resolver::EncodableResolve = resolve.try_into()?;
         Ok(Some(v.into_resolve(&s, ws)?))
     })()
@@ -101,7 +100,7 @@ fn resolve_to_string_orig(
 }
 
 fn serialize_resolve(resolve: &Resolve, orig: Option<&str>) -> String {
-    let toml = toml_edit::ser::to_item(resolve).unwrap();
+    let toml = toml::Table::try_from(resolve).unwrap();
 
     let mut out = String::new();
 
@@ -140,7 +139,7 @@ fn serialize_resolve(resolve: &Resolve,
 
     let deps = toml["package"].as_array().unwrap();
     for dep in deps {
-        let dep = dep.as_inline_table().unwrap();
+        let dep = dep.as_table().unwrap();
 
         out.push_str("[[package]]\n");
         emit_package(dep, &mut out);
@@ -150,7 +149,7 @@ fn serialize_resolve(resolve: &Resolve,
         let list = patch["unused"].as_array().unwrap();
         for entry in list {
             out.push_str("[[patch.unused]]\n");
-            emit_package(entry.as_inline_table().unwrap(), &mut out);
+            emit_package(entry.as_table().unwrap(), &mut out);
             out.push('\n');
         }
     }
@@ -160,11 +159,11 @@ fn serialize_resolve(resolve: &Resolve,
         //    (which `toml_edit::Table::to_string` only shows)
         // 2. We need to ensure all children tables have `metadata.` prefix
         let meta_table = meta
-            .clone()
-            .into_table()
-            .expect("validation ensures this is a table");
-        let mut meta_doc = toml_edit::Document::new();
-        meta_doc["metadata"] = toml_edit::Item::Table(meta_table);
+            .as_table()
+            .expect("validation ensures this is a table")
+            .clone();
+        let mut meta_doc = toml::Table::new();
+        meta_doc.insert("metadata".to_owned(), toml::Value::Table(meta_table));
 
         out.push_str(&meta_doc.to_string());
     }
@@ -200,7 +199,7 @@ fn are_equal_lockfiles(orig: &str, curre
     orig.lines().eq(current.lines())
 }
 
-fn emit_package(dep: &toml_edit::InlineTable, out: &mut String) {
+fn emit_package(dep: &toml::Table, out: &mut String) {
     out.push_str(&format!("name = {}\n", &dep["name"]));
     out.push_str(&format!("version = {}\n", &dep["version"]));
 
Index: cargo/src/cargo/ops/vendor.rs
===================================================================
--- cargo.orig/src/cargo/ops/vendor.rs
+++ cargo/src/cargo/ops/vendor.rs
@@ -12,7 +12,6 @@ use std::collections::{BTreeMap, BTreeSe
 use std::fs::{self, File, OpenOptions};
 use std::io::{Read, Write};
 use std::path::{Path, PathBuf};
-use toml_edit::easy as toml;
 
 pub struct VendorOptions<'a> {
     pub no_delete: bool,
Index: cargo/src/cargo/util/config/key.rs
===================================================================
--- cargo.orig/src/cargo/util/config/key.rs
+++ cargo/src/cargo/util/config/key.rs
@@ -111,6 +111,6 @@ fn escape_key_part<'a>(part: &'a str) ->
         Cow::Borrowed(part)
     } else {
         // This is a bit messy, but toml doesn't expose a function to do this.
-        Cow::Owned(toml_edit::Value::from(part).to_string())
+        Cow::Owned(toml::Value::from(part).to_string())
     }
 }
Index: cargo/src/cargo/util/config/mod.rs
===================================================================
--- cargo.orig/src/cargo/util/config/mod.rs
+++ cargo/src/cargo/util/config/mod.rs
@@ -79,7 +79,8 @@ use cargo_util::paths;
 use curl::easy::Easy;
 use lazycell::LazyCell;
 use serde::Deserialize;
-use toml_edit::{easy as toml, Item};
+use serde::de::IntoDeserializer as _;
+use toml_edit::Item;
 use url::Url;
 
 mod de;
@@ -826,17 +827,11 @@ impl Config {
         let def = Definition::Environment(key.as_env_key().to_string());
         if self.cli_unstable().advanced_env && env_val.starts_with('[') && env_val.ends_with(']') {
             // Parse an environment string as a TOML array.
-            let toml_s = format!("value={}", env_val);
-            let toml_v: toml::Value = toml::de::from_str(&toml_s).map_err(|e| {
-                ConfigError::new(format!("could not parse TOML list: {}", e), def.clone())
-            })?;
-            let values = toml_v
-                .as_table()
-                .unwrap()
-                .get("value")
-                .unwrap()
-                .as_array()
-                .expect("env var was not array");
+            let toml_v = toml::Value::deserialize(toml::de::ValueDeserializer::new(&env_val))
+                .map_err(|e| {
+                    ConfigError::new(format!("could not parse TOML list: {}", e), def.clone())
+                })?;
+            let values = toml_v.as_array().expect("env var was not array");
             for value in values {
                 // TODO: support other types.
                 let s = value.as_str().ok_or_else(|| {
@@ -1089,11 +1084,11 @@ impl Config {
         }
         let contents = fs::read_to_string(path)
             .with_context(|| format!("failed to read configuration file `{}`", path.display()))?;
-        let toml = cargo_toml::parse(&contents, path, self).with_context(|| {
+        let toml = cargo_toml::parse_document(&contents, path, self).with_context(|| {
             format!("could not parse TOML configuration in `{}`", path.display())
         })?;
         let value =
-            CV::from_toml(Definition::Path(path.to_path_buf()), toml).with_context(|| {
+            CV::from_toml(Definition::Path(path.to_path_buf()), toml::Value::Table(toml)).with_context(|| {
                 format!(
                     "failed to load TOML configuration from `{}`",
                     path.display()
@@ -1202,8 +1197,10 @@ impl Config {
                     format!("failed to parse value from --config argument `{arg}` as a dotted key expression")
                 })?;
                 fn non_empty_decor(d: &toml_edit::Decor) -> bool {
-                    d.prefix().map_or(false, |p| !p.trim().is_empty())
-                        || d.suffix().map_or(false, |s| !s.trim().is_empty())
+                    d.prefix()
+                        .map_or(false, |p| !p.as_str().unwrap_or_default().trim().is_empty())
+                        || d.suffix()
+                            .map_or(false, |s| !s.as_str().unwrap_or_default().trim().is_empty())
                 }
                 let ok = {
                     let mut got_to_value = false;
@@ -1263,9 +1260,10 @@ impl Config {
                     );
                 }
 
-                let toml_v: toml::Value = toml::from_document(doc).with_context(|| {
-                    format!("failed to parse value from --config argument `{arg}`")
-                })?;
+                let toml_v: toml::Value = toml::Value::deserialize(doc.into_deserializer())
+                    .with_context(|| {
+                        format!("failed to parse value from --config argument `{arg}`")
+                    })?;
 
                 if toml_v
                     .get("registry")
@@ -2023,14 +2021,12 @@ pub fn save_credentials(
         )
     })?;
 
-    let mut toml = cargo_toml::parse(&contents, file.path(), cfg)?;
+    let mut toml = cargo_toml::parse_document(&contents, file.path(), cfg)?;
 
     // Move the old token location to the new one.
-    if let Some(token) = toml.as_table_mut().unwrap().remove("token") {
+    if let Some(token) = toml.remove("token") {
         let map = HashMap::from([("token".to_string(), token)]);
-        toml.as_table_mut()
-            .unwrap()
-            .insert("registry".into(), map.into());
+        toml.insert("registry".into(), map.into());
     }
 
     if let Some(token) = token {
@@ -2053,17 +2049,16 @@ pub fn save_credentials(
         };
 
         if registry.is_some() {
-            if let Some(table) = toml.as_table_mut().unwrap().remove("registries") {
+            if let Some(table) = toml.remove("registries") {
                 let v = CV::from_toml(Definition::Path(file.path().to_path_buf()), table)?;
                 value.merge(v, false)?;
             }
         }
-        toml.as_table_mut().unwrap().insert(key, value.into_toml());
+        toml.insert(key, value.into_toml());
     } else {
         // logout
-        let table = toml.as_table_mut().unwrap();
         if let Some(registry) = registry {
-            if let Some(registries) = table.get_mut("registries") {
+            if let Some(registries) = toml.get_mut("registries") {
                 if let Some(reg) = registries.get_mut(registry) {
                     let rtable = reg.as_table_mut().ok_or_else(|| {
                         format_err!("expected `[registries.{}]` to be a table", registry)
@@ -2071,7 +2066,7 @@ pub fn save_credentials(
                     rtable.remove("token");
                 }
             }
-        } else if let Some(registry) = table.get_mut("registry") {
+        } else if let Some(registry) = toml.get_mut("registry") {
             let reg_table = registry
                 .as_table_mut()
                 .ok_or_else(|| format_err!("expected `[registry]` to be a table"))?;
Index: cargo/src/cargo/util/config/target.rs
===================================================================
--- cargo.orig/src/cargo/util/config/target.rs
+++ cargo/src/cargo/util/config/target.rs
@@ -4,7 +4,6 @@ use crate::util::CargoResult;
 use serde::Deserialize;
 use std::collections::{BTreeMap, HashMap};
 use std::path::PathBuf;
-use toml_edit::easy as toml;
 
 /// Config definition of a `[target.'cfg(…)']` table.
 ///
Index: cargo/src/cargo/util/toml/mod.rs
===================================================================
--- cargo.orig/src/cargo/util/toml/mod.rs
+++ cargo/src/cargo/util/toml/mod.rs
@@ -12,9 +12,9 @@ use lazycell::LazyCell;
 use log::{debug, trace};
 use semver::{self, VersionReq};
 use serde::de;
+use serde::de::IntoDeserializer as _;
 use serde::ser;
 use serde::{Deserialize, Serialize};
-use toml_edit::easy as toml;
 use url::Url;
 
 use crate::core::compiler::{CompileKind, CompileTarget};
@@ -36,9 +36,6 @@ use crate::util::{
 mod targets;
 use self::targets::targets;
 
-pub use toml_edit::de::Error as TomlDeError;
-pub use toml_edit::TomlError as TomlEditError;
-
 /// Loads a `Cargo.toml` from a file on disk.
 ///
 /// This could result in a real or virtual manifest being returned.
@@ -90,21 +87,16 @@ pub fn read_manifest_from_str(
     // Provide a helpful error message for a common user error.
     if let Some(package) = toml.get("package").or_else(|| toml.get("project")) {
         if let Some(feats) = package.get("cargo-features") {
-            let mut feats = feats.clone();
-            if let Some(value) = feats.as_value_mut() {
-                // Only keep formatting inside of the `[]` and not formatting around it
-                value.decor_mut().clear();
-            }
             bail!(
                 "cargo-features = {} was found in the wrong location: it \
                  should be set at the top of Cargo.toml before any tables",
-                feats.to_string()
+                feats
             );
         }
     }
 
     let mut unused = BTreeSet::new();
-    let manifest: TomlManifest = serde_ignored::deserialize(toml, |path| {
+    let manifest: TomlManifest = serde_ignored::deserialize(toml.into_deserializer(), |path| {
         let mut key = String::new();
         stringify(&mut key, &path);
         unused.insert(key);
@@ -186,23 +178,7 @@ pub fn read_manifest_from_str(
     }
 }
 
-/// Attempts to parse a string into a [`toml::Value`]. This is not specific to any
-/// particular kind of TOML file.
-///
-/// The purpose of this wrapper is to detect invalid TOML which was previously
-/// accepted and display a warning to the user in that case. The `file` and `config`
-/// parameters are only used by this fallback path.
-pub fn parse(toml: &str, _file: &Path, _config: &Config) -> CargoResult<toml::Value> {
-    // At the moment, no compatibility checks are needed.
-    toml.parse()
-        .map_err(|e| anyhow::Error::from(e).context("could not parse input as TOML"))
-}
-
-pub fn parse_document(
-    toml: &str,
-    _file: &Path,
-    _config: &Config,
-) -> CargoResult<toml_edit::Document> {
+pub fn parse_document(toml: &str, _file: &Path, _config: &Config) -> CargoResult<toml::Table> {
     // At the moment, no compatibility checks are needed.
     toml.parse()
         .map_err(|e| anyhow::Error::from(e).context("could not parse input as TOML"))
Index: cargo/tests/testsuite/bad_config.rs
===================================================================
--- cargo.orig/tests/testsuite/bad_config.rs
+++ cargo/tests/testsuite/bad_config.rs
@@ -174,8 +174,7 @@ Caused by:
     |
   1 | 4
     |  ^
-  Unexpected end of input
-  Expected `.` or `=`
+  expected `.`, `=`
 ",
         )
         .run();
@@ -195,7 +194,8 @@ fn bad_cargo_lock() {
 [ERROR] failed to parse lock file at: [..]Cargo.lock
 
 Caused by:
-  missing field `name` for key `package`
+  missing field `name`
+  in `package`
 ",
         )
         .run();
@@ -298,7 +298,8 @@ fn bad_source_in_cargo_lock() {
 [ERROR] failed to parse lock file at: [..]
 
 Caused by:
-  invalid source `You shall not parse` for key `package.source`
+  invalid source `You shall not parse`
+  in `package.source`
 ",
         )
         .run();
@@ -425,9 +426,8 @@ Caused by:
     |
   8 |                 native = {
     |                           ^
-  Unexpected `
-  `
-  Expected key
+  invalid inline table
+  expected `}`
 ",
         )
         .run();
@@ -781,9 +781,8 @@ Caused by:
     |
   1 | [bar] baz = 2
     |       ^
-  Unexpected `b`
-  Expected newline or end of input
-  While parsing a Table Header
+  invalid table header
+  expected newline, `#`
 ",
         )
         .run();
@@ -1244,6 +1243,7 @@ error: failed to parse manifest at `[..]
 
 Caused by:
   invalid type: integer `3`, expected a version string like [..]
+  in `dependencies.bar`
 ",
         )
         .run();
@@ -1274,7 +1274,8 @@ fn bad_debuginfo() {
 error: failed to parse manifest at `[..]`
 
 Caused by:
-  expected a boolean or an integer for [..]
+  expected a boolean or an integer
+  in `profile.dev.debug`
 ",
         )
         .run();
@@ -1303,7 +1304,8 @@ fn bad_opt_level() {
 error: failed to parse manifest at `[..]`
 
 Caused by:
-  expected a boolean or a string for key [..]
+  expected a boolean or a string
+  in `package.build`
 ",
         )
         .run();
Index: cargo/tests/testsuite/build.rs
===================================================================
--- cargo.orig/tests/testsuite/build.rs
+++ cargo/tests/testsuite/build.rs
@@ -202,8 +202,8 @@ Caused by:
     |
   3 |                 foo = bar
     |                       ^
-  Unexpected `b`
-  Expected quoted string
+  invalid string
+  expected `\"`, `'`
 ",
         )
         .run();
@@ -227,8 +227,8 @@ Caused by:
     |
   1 | a = bar
     |     ^
-  Unexpected `b`
-  Expected quoted string
+  invalid string
+  expected `\"`, `'`
 ",
         )
         .run();
@@ -280,7 +280,8 @@ fn cargo_compile_with_invalid_version()
 [ERROR] failed to parse manifest at `[..]`
 
 Caused by:
-  unexpected end of input while parsing minor version number for key `package.version`
+  unexpected end of input while parsing minor version number
+  in `package.version`
 ",
         )
         .run();
@@ -2951,8 +2952,7 @@ Caused by:
     |
   1 | this is not valid toml
     |      ^
-  Unexpected `i`
-  Expected `.` or `=`
+  expected `.`, `=`
 ",
         )
         .run();
Index: cargo/tests/testsuite/cargo_add/invalid_manifest/stderr.log
===================================================================
--- cargo.orig/tests/testsuite/cargo_add/invalid_manifest/stderr.log
+++ cargo/tests/testsuite/cargo_add/invalid_manifest/stderr.log
@@ -8,4 +8,5 @@ Caused by:
     |
   8 | key = invalid-value
     |       ^
-  Unexpected `v`
+  invalid string
+  expected `"`, `'`
Index: cargo/tests/testsuite/config.rs
===================================================================
--- cargo.orig/tests/testsuite/config.rs
+++ cargo/tests/testsuite/config.rs
@@ -3,7 +3,7 @@
 use cargo::core::{PackageIdSpec, Shell};
 use cargo::util::config::{self, Config, Definition, SslVersionConfig, StringList};
 use cargo::util::interning::InternedString;
-use cargo::util::toml::{self, VecStringOrBool as VSOB};
+use cargo::util::toml::{self as cargo_toml, VecStringOrBool as VSOB};
 use cargo::CargoResult;
 use cargo_test_support::compare;
 use cargo_test_support::{panic_error, paths, project, symlink_supported, t};
@@ -352,17 +352,19 @@ lto = false
         .build();
 
     // TODO: don't use actual `tomlprofile`.
-    let p: toml::TomlProfile = config.get("profile.dev").unwrap();
+    let p: cargo_toml::TomlProfile = config.get("profile.dev").unwrap();
     let mut packages = BTreeMap::new();
-    let key = toml::ProfilePackageSpec::Spec(::cargo::core::PackageIdSpec::parse("bar").unwrap());
-    let o_profile = toml::TomlProfile {
-        opt_level: Some(toml::TomlOptLevel("2".to_string())),
+    let key =
+        cargo_toml::ProfilePackageSpec::Spec(::cargo::core::PackageIdSpec::parse("bar").unwrap());
+    let o_profile = cargo_toml::TomlProfile {
+        opt_level: Some(cargo_toml::TomlOptLevel("2".to_string())),
         codegen_units: Some(9),
         ..Default::default()
     };
     packages.insert(key, o_profile);
-    let key = toml::ProfilePackageSpec::Spec(::cargo::core::PackageIdSpec::parse("env").unwrap());
-    let o_profile = toml::TomlProfile {
+    let key =
+        cargo_toml::ProfilePackageSpec::Spec(::cargo::core::PackageIdSpec::parse("env").unwrap());
+    let o_profile = cargo_toml::TomlProfile {
         codegen_units: Some(13),
         ..Default::default()
     };
@@ -370,19 +372,19 @@ lto = false
 
     assert_eq!(
         p,
-        toml::TomlProfile {
-            opt_level: Some(toml::TomlOptLevel("s".to_string())),
-            lto: Some(toml::StringOrBool::Bool(true)),
+        cargo_toml::TomlProfile {
+            opt_level: Some(cargo_toml::TomlOptLevel("s".to_string())),
+            lto: Some(cargo_toml::StringOrBool::Bool(true)),
             codegen_units: Some(5),
-            debug: Some(toml::U32OrBool::Bool(true)),
+            debug: Some(cargo_toml::U32OrBool::Bool(true)),
             debug_assertions: Some(true),
             rpath: Some(true),
             panic: Some("abort".to_string()),
             overflow_checks: Some(true),
             incremental: Some(true),
             package: Some(packages),
-            build_override: Some(Box::new(toml::TomlProfile {
-                opt_level: Some(toml::TomlOptLevel("1".to_string())),
+            build_override: Some(Box::new(cargo_toml::TomlProfile {
+                opt_level: Some(cargo_toml::TomlOptLevel("1".to_string())),
                 codegen_units: Some(11),
                 ..Default::default()
             })),
@@ -390,11 +392,11 @@ lto = false
         }
     );
 
-    let p: toml::TomlProfile = config.get("profile.no-lto").unwrap();
+    let p: cargo_toml::TomlProfile = config.get("profile.no-lto").unwrap();
     assert_eq!(
         p,
-        toml::TomlProfile {
-            lto: Some(toml::StringOrBool::Bool(false)),
+        cargo_toml::TomlProfile {
+            lto: Some(cargo_toml::StringOrBool::Bool(false)),
             dir_name: Some(InternedString::new("without-lto")),
             inherits: Some(InternedString::new("dev")),
             ..Default::default()
@@ -408,24 +410,24 @@ fn profile_env_var_prefix() {
     let config = ConfigBuilder::new()
         .env("CARGO_PROFILE_DEV_DEBUG_ASSERTIONS", "false")
         .build();
-    let p: toml::TomlProfile = config.get("profile.dev").unwrap();
+    let p: cargo_toml::TomlProfile = config.get("profile.dev").unwrap();
     assert_eq!(p.debug_assertions, Some(false));
     assert_eq!(p.debug, None);
 
     let config = ConfigBuilder::new()
         .env("CARGO_PROFILE_DEV_DEBUG", "1")
         .build();
-    let p: toml::TomlProfile = config.get("profile.dev").unwrap();
+    let p: cargo_toml::TomlProfile = config.get("profile.dev").unwrap();
     assert_eq!(p.debug_assertions, None);
-    assert_eq!(p.debug, Some(toml::U32OrBool::U32(1)));
+    assert_eq!(p.debug, Some(cargo_toml::U32OrBool::U32(1)));
 
     let config = ConfigBuilder::new()
         .env("CARGO_PROFILE_DEV_DEBUG_ASSERTIONS", "false")
         .env("CARGO_PROFILE_DEV_DEBUG", "1")
         .build();
-    let p: toml::TomlProfile = config.get("profile.dev").unwrap();
+    let p: cargo_toml::TomlProfile = config.get("profile.dev").unwrap();
     assert_eq!(p.debug_assertions, Some(false));
-    assert_eq!(p.debug, Some(toml::U32OrBool::U32(1)));
+    assert_eq!(p.debug, Some(cargo_toml::U32OrBool::U32(1)));
 }
 
 #[cargo_test]
@@ -542,7 +544,9 @@ opt-level = 'foo'
     let config = new_config();
 
     assert_error(
-        config.get::<toml::TomlProfile>("profile.dev").unwrap_err(),
+        config
+            .get::<cargo_toml::TomlProfile>("profile.dev")
+            .unwrap_err(),
         "\
 error in [..]/.cargo/config: could not load config key `profile.dev.opt-level`
 
@@ -555,7 +559,7 @@ Caused by:
         .build();
 
     assert_error(
-        config.get::<toml::TomlProfile>("profile.dev").unwrap_err(),
+        config.get::<cargo_toml::TomlProfile>("profile.dev").unwrap_err(),
         "\
 error in environment variable `CARGO_PROFILE_DEV_OPT_LEVEL`: could not load config key `profile.dev.opt-level`
 
@@ -700,8 +704,7 @@ Caused by:
   |
 1 | asdf
   |     ^
-Unexpected end of input
-Expected `.` or `=`",
+expected `.`, `=`",
     );
 }
 
@@ -771,12 +774,12 @@ expected a list, but found a integer for
     assert_error(
         config.get::<L>("bad-env").unwrap_err(),
         "\
-error in environment variable `CARGO_BAD_ENV`: could not parse TOML list: TOML parse error at line 1, column 8
+error in environment variable `CARGO_BAD_ENV`: could not parse TOML list: TOML parse error at line 1, column 2
   |
-1 | value=[zzz]
-  |        ^
-Unexpected `z`
-Expected newline or `#`
+1 | [zzz]
+  |  ^
+invalid array
+expected `]`
 ",
     );
 
@@ -1070,8 +1073,7 @@ Caused by:
   |
 3 | ssl-version.min = 'tlsv1.2'
   | ^
-Dotted key `ssl-version` attempted to extend non-table type (string)
-
+dotted key `ssl-version` attempted to extend non-table type (string)
 ",
     );
     assert!(config
@@ -1444,9 +1446,12 @@ strip = 'debuginfo'
 
     let config = new_config();
 
-    let p: toml::TomlProfile = config.get("profile.release").unwrap();
+    let p: cargo_toml::TomlProfile = config.get("profile.release").unwrap();
     let strip = p.strip.unwrap();
-    assert_eq!(strip, toml::StringOrBool::String("debuginfo".to_string()));
+    assert_eq!(
+        strip,
+        cargo_toml::StringOrBool::String("debuginfo".to_string())
+    );
 }
 
 #[cargo_test]
@@ -1480,12 +1485,12 @@ fn cargo_target_empty_env() {
 #[cargo_test]
 fn all_profile_options() {
     // Check that all profile options can be serialized/deserialized.
-    let base_settings = toml::TomlProfile {
-        opt_level: Some(toml::TomlOptLevel("0".to_string())),
-        lto: Some(toml::StringOrBool::String("thin".to_string())),
+    let base_settings = cargo_toml::TomlProfile {
+        opt_level: Some(cargo_toml::TomlOptLevel("0".to_string())),
+        lto: Some(cargo_toml::StringOrBool::String("thin".to_string())),
         codegen_backend: Some(InternedString::new("example")),
         codegen_units: Some(123),
-        debug: Some(toml::U32OrBool::U32(1)),
+        debug: Some(cargo_toml::U32OrBool::U32(1)),
         split_debuginfo: Some("packed".to_string()),
         debug_assertions: Some(true),
         rpath: Some(true),
@@ -1494,22 +1499,22 @@ fn all_profile_options() {
         incremental: Some(true),
         dir_name: Some(InternedString::new("dir_name")),
         inherits: Some(InternedString::new("debug")),
-        strip: Some(toml::StringOrBool::String("symbols".to_string())),
+        strip: Some(cargo_toml::StringOrBool::String("symbols".to_string())),
         package: None,
         build_override: None,
         rustflags: None,
     };
     let mut overrides = BTreeMap::new();
-    let key = toml::ProfilePackageSpec::Spec(PackageIdSpec::parse("foo").unwrap());
+    let key = cargo_toml::ProfilePackageSpec::Spec(PackageIdSpec::parse("foo").unwrap());
     overrides.insert(key, base_settings.clone());
-    let profile = toml::TomlProfile {
+    let profile = cargo_toml::TomlProfile {
         build_override: Some(Box::new(base_settings.clone())),
         package: Some(overrides),
         ..base_settings
     };
-    let profile_toml = toml_edit::easy::to_string(&profile).unwrap();
-    let roundtrip: toml::TomlProfile = toml_edit::easy::from_str(&profile_toml).unwrap();
-    let roundtrip_toml = toml_edit::easy::to_string(&roundtrip).unwrap();
+    let profile_toml = toml::to_string(&profile).unwrap();
+    let roundtrip: cargo_toml::TomlProfile = toml::from_str(&profile_toml).unwrap();
+    let roundtrip_toml = toml::to_string(&roundtrip).unwrap();
     compare::assert_match_exact(&profile_toml, &roundtrip_toml);
 }
 
Index: cargo/tests/testsuite/config_cli.rs
===================================================================
--- cargo.orig/tests/testsuite/config_cli.rs
+++ cargo/tests/testsuite/config_cli.rs
@@ -327,8 +327,7 @@ Caused by:
   |
 1 | abc
   |    ^
-Unexpected end of input
-Expected `.` or `=`
+expected `.`, `=`
 ",
     );
 
@@ -477,8 +476,7 @@ Caused by:
   |
 1 | missing.toml
   |             ^
-Unexpected end of input
-Expected `.` or `=`
+expected `.`, `=`
 ",
     );
 }
Index: cargo/tests/testsuite/inheritable_workspace_fields.rs
===================================================================
--- cargo.orig/tests/testsuite/inheritable_workspace_fields.rs
+++ cargo/tests/testsuite/inheritable_workspace_fields.rs
@@ -1210,8 +1210,8 @@ Caused by:
     |
   3 |             members = [invalid toml
     |                        ^
-  Unexpected `i`
-  Expected newline or `#`
+  invalid array
+  expected `]`
 ",
         )
         .run();
Index: cargo/tests/testsuite/install.rs
===================================================================
--- cargo.orig/tests/testsuite/install.rs
+++ cargo/tests/testsuite/install.rs
@@ -991,8 +991,7 @@ Caused by:
     |
   1 | [..]
     | ^
-  Unexpected `[..]`
-  Expected key or end of input
+  invalid key
 ",
         )
         .run();
Index: cargo/tests/testsuite/install_upgrade.rs
===================================================================
--- cargo.orig/tests/testsuite/install_upgrade.rs
+++ cargo/tests/testsuite/install_upgrade.rs
@@ -6,7 +6,6 @@ use std::env;
 use std::fs;
 use std::path::PathBuf;
 use std::sync::atomic::{AtomicUsize, Ordering};
-use toml_edit::easy as toml;
 
 use cargo_test_support::install::{cargo_home, exe};
 use cargo_test_support::paths::CargoPathExt;
Index: cargo/tests/testsuite/login.rs
===================================================================
--- cargo.orig/tests/testsuite/login.rs
+++ cargo/tests/testsuite/login.rs
@@ -5,7 +5,6 @@ use cargo_test_support::registry::Regist
 use cargo_test_support::{cargo_process, t};
 use std::fs::{self};
 use std::path::PathBuf;
-use toml_edit::easy as toml;
 
 const TOKEN: &str = "test-token";
 const TOKEN2: &str = "test-token2";
@@ -29,12 +28,12 @@ fn check_token(expected_token: &str, reg
     assert!(credentials.is_file());
 
     let contents = fs::read_to_string(&credentials).unwrap();
-    let toml: toml::Value = contents.parse().unwrap();
+    let toml: toml::Table = contents.parse().unwrap();
 
-    let token = match (registry, toml) {
+    let token = match registry {
         // A registry has been provided, so check that the token exists in a
         // table for the registry.
-        (Some(registry), toml::Value::Table(table)) => table
+        Some(registry) => toml
             .get("registries")
             .and_then(|registries_table| registries_table.get(registry))
             .and_then(|registry_table| match registry_table.get("token") {
@@ -42,14 +41,13 @@ fn check_token(expected_token: &str, reg
                 _ => None,
             }),
         // There is no registry provided, so check the global token instead.
-        (None, toml::Value::Table(table)) => table
+        None => toml
             .get("registry")
             .and_then(|registry_table| registry_table.get("token"))
             .and_then(|v| match v {
                 toml::Value::String(ref token) => Some(token.as_str().to_string()),
                 _ => None,
             }),
-        _ => None,
     };
 
     if let Some(token_val) = token {
Index: cargo/tests/testsuite/logout.rs
===================================================================
--- cargo.orig/tests/testsuite/logout.rs
+++ cargo/tests/testsuite/logout.rs
@@ -3,7 +3,6 @@
 use cargo_test_support::install::cargo_home;
 use cargo_test_support::{cargo_process, registry};
 use std::fs;
-use toml_edit::easy as toml;
 
 #[cargo_test]
 fn gated() {
@@ -25,7 +24,7 @@ the `cargo logout` command.
 fn check_config_token(registry: Option<&str>, should_be_set: bool) {
     let credentials = cargo_home().join("credentials");
     let contents = fs::read_to_string(&credentials).unwrap();
-    let toml: toml::Value = contents.parse().unwrap();
+    let toml: toml::Table = contents.parse().unwrap();
     if let Some(registry) = registry {
         assert_eq!(
             toml.get("registries")
Index: cargo/tests/testsuite/patch.rs
===================================================================
--- cargo.orig/tests/testsuite/patch.rs
+++ cargo/tests/testsuite/patch.rs
@@ -5,7 +5,6 @@ use cargo_test_support::paths;
 use cargo_test_support::registry::{self, Package};
 use cargo_test_support::{basic_manifest, project};
 use std::fs;
-use toml_edit::easy as toml;
 
 #[cargo_test]
 fn replace() {
@@ -386,7 +385,7 @@ version. [..]
 
     // unused patch should be in the lock file
     let lock = p.read_lockfile();
-    let toml: toml::Value = toml::from_str(&lock).unwrap();
+    let toml: toml::Table = toml::from_str(&lock).unwrap();
     assert_eq!(toml["patch"]["unused"].as_array().unwrap().len(), 1);
     assert_eq!(toml["patch"]["unused"][0]["name"].as_str(), Some("bar"));
     assert_eq!(
@@ -494,7 +493,7 @@ fn prefer_patch_version() {
 
     // there should be no patch.unused in the toml file
     let lock = p.read_lockfile();
-    let toml: toml::Value = toml::from_str(&lock).unwrap();
+    let toml: toml::Table = toml::from_str(&lock).unwrap();
     assert!(toml.get("patch").is_none());
 }
 
@@ -559,7 +558,7 @@ version. [..]
 
     // unused patch should be in the lock file
     let lock = p.read_lockfile();
-    let toml: toml::Value = toml::from_str(&lock).unwrap();
+    let toml: toml::Table = toml::from_str(&lock).unwrap();
     assert_eq!(toml["patch"]["unused"].as_array().unwrap().len(), 1);
     assert_eq!(toml["patch"]["unused"][0]["name"].as_str(), Some("bar"));
     assert_eq!(
@@ -1934,7 +1933,7 @@ fn update_unused_new_version() {
         .run();
     // unused patch should be in the lock file
     let lock = p.read_lockfile();
-    let toml: toml::Value = toml::from_str(&lock).unwrap();
+    let toml: toml::Table = toml::from_str(&lock).unwrap();
     assert_eq!(toml["patch"]["unused"].as_array().unwrap().len(), 1);
     assert_eq!(toml["patch"]["unused"][0]["name"].as_str(), Some("bar"));
     assert_eq!(
Index: cargo/tests/testsuite/workspaces.rs
===================================================================
--- cargo.orig/tests/testsuite/workspaces.rs
+++ cargo/tests/testsuite/workspaces.rs
@@ -1098,8 +1098,7 @@ Caused by:
     |
   1 | asdf
     |     ^
-  Unexpected end of input
-  Expected `.` or `=`
+  expected `.`, `=`
      Created binary (application) `bar` package
 ",
         )
Index: cargo/Cargo.toml
===================================================================
--- cargo.orig/Cargo.toml
+++ cargo/Cargo.toml
@@ -189,12 +189,10 @@ version = "3.0"
 version = "1.1"
 
 [dependencies.toml_edit]
-version = "0.14.3"
-features = [
-    "serde",
-    "easy",
-    "perf",
-]
+version = "0.19"
+
+[dependencies.toml]
+version = "0.7.0"
 
 [dependencies.unicode-width]
 version = "0.1.5"
